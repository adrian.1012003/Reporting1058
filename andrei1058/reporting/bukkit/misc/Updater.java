package com.andrei1058.reporting.bukkit.misc;

import org.bukkit.Bukkit;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import static com.andrei1058.reporting.Main.newVersion;
import static com.andrei1058.reporting.Main.plugin;

/**
 * Copyright Andrei Dascalu - andrei1058 @spigotmc.org
 * Reporting1058 class written on 08/02/2017
 */
public class Updater {
    public static void checkUpdates(){
        Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
            @Override
            public void run() {
                try {
                    HttpURLConnection checkUpdate = (HttpURLConnection) new URL("http://www.spigotmc.org/api/general.php").openConnection();
                    checkUpdate.setDoOutput(true);
                    checkUpdate.setRequestMethod("POST");
                    checkUpdate.getOutputStream().write(("key=98BE0FE67F88AB82B4C197FAF1DC3B69206EFDCC4D3B80FC83A00037510B99B4&resource=19751").getBytes());
                    String old = plugin.getDescription().getVersion();
                    String newV =  new BufferedReader(new InputStreamReader(checkUpdate.getInputStream())).readLine().replaceAll("[a-zA-Z ]", "");
                    if (!newV.equalsIgnoreCase(old)){
                        plugin.update = true;
                        newVersion = newV;
                        plugin.getLogger().info("There is a nev version available!");
                        plugin.getLogger().info("["+newVersion+"] www.spigotmc.org/resources/19751/");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }, 30L);
    }
}
