package com.andrei1058.reporting.bukkit.misc;

import com.andrei1058.reporting.bukkit.settings.Config;
import com.andrei1058.reporting.bukkit.settings.Database;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import static com.andrei1058.reporting.Main.*;

public class Listeners implements Listener {

    @EventHandler
    public void j(PlayerJoinEvent e) {
        if (mysql && motd) {
            Bukkit.getScheduler().runTaskLater(plugin, () -> {
                if (e.getPlayer().hasPermission("reporting.motd")) {
                    MySQL m = new MySQL(Database.host, Database.port, Database.database, Database.username, Database.password);
                    int x = m.getActiveNr();
                    if (x != 0) {
                        e.getPlayer().sendMessage(Config.current_server_active_reports.replace("{reports}", String.valueOf(x)));
                    }
                }
            }, 40L);
        }
        if (update){
            if (e.getPlayer().isOp()){
                e.getPlayer().sendMessage("§9Reporting1058 §8» §aThere is a new version available!");
                e.getPlayer().sendMessage("§9[§7"+newVersion+"§9] §ewww.spigotmc.org/resources/19751/");
            }
        }
    }
}
