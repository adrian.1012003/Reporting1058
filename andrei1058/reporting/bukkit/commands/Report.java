package com.andrei1058.reporting.bukkit.commands;

import com.andrei1058.reporting.bukkit.misc.MySQL;
import com.andrei1058.reporting.bukkit.misc.SQLite;
import com.andrei1058.reporting.bukkit.misc.Titles;
import com.andrei1058.reporting.bukkit.settings.Config;
import com.andrei1058.reporting.bukkit.settings.Database;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.UUID;

import static com.andrei1058.reporting.Main.*;

public class Report implements CommandExecutor {

    private static HashMap<UUID, Long> cooldown = new HashMap();

    @Override
    public boolean onCommand(CommandSender s, Command c, String st, String[] args) {
        if (c.getName().equalsIgnoreCase("report"))
        if (s instanceof ConsoleCommandSender) return true;
        Player p = (Player) s;
        if (args.length <= 1) {
            p.sendMessage(Config.insuficientargs);
            return true;
        }
        if (p.getName().equalsIgnoreCase(args[0])){
            p.sendMessage(Config.reporturself);
            return true;
        }
        if (disabled_worlds.contains(p.getWorld().getName())){
            s.sendMessage(Config.disabled_world);
            return true;
        }
        if (cooldown.containsKey(p.getUniqueId())){
            if (System.currentTimeMillis() - rep_cooldown < cooldown.get(p.getUniqueId())){
                s.sendMessage(Config.report_cooldown);
                return true;
            } else {
                cooldown.remove(p.getUniqueId());
            }
        }
        if (Bukkit.getPlayer(args[0]) == null) {
            p.sendMessage(Config.offlinereported.replace("{player}", args[0]));
            return true;
        }
        if (Bukkit.getPlayer(args[0]).hasPermission("reporting.antireport") ||
                Bukkit.getPlayer(args[0]).hasPermission("reporting.*")){
            p.sendMessage(Config.anti_report);
            return true;
        }
        StringBuilder reason = new StringBuilder();
        for (int i = 1; i < args.length; i++) {
            reason.append(args[i] + " ").toString();
        }
        p.sendMessage(Config.reportsent);
        for (Player admin : Bukkit.getOnlinePlayers()){
            if (admin.hasPermission("reporting.receive") || admin.hasPermission("reporting.*")) {
                if (plugin.titles){
                    Titles.sendTitle(admin, 0, 20, 0, Config.titletitle, Config.titlesubtitle.replace("{reported}", args[0]).replace("{reporter}", p.getName()));
                }
                if (plugin.action){
                    Titles.sendAction(admin,Config.actionmsg.replace("{reported}", args[0]).replace("{reason}", reason).replace("{reporter}", p.getName()));
                }
                admin.sendMessage(Config.chatmsg.replace("{reported}", args[0]).replace("{reporter}", p.getName()).replace("{reason}", reason.toString())
                        .replace("{server}", p.getServer().getName()));
            }
        }
        cooldown.put(p.getUniqueId(), System.currentTimeMillis());
            int i = 0;
            if (Bukkit.getPlayer(args[0]) != null) {
                if (mysql) {
                    MySQL m = new MySQL(Database.host, Database.port, Database.database, Database.username, Database.password);
                    if (m.isDataExists(Database.playertable, "Player", Bukkit.getPlayer(args[0]).getUniqueId().toString())) {
                        i = (int) new MySQL(Database.host, Database.port, Database.database, Database.username,
                                Database.password).getReportedTimes(Database.playertable, Bukkit.getPlayer(args[0]).getUniqueId());
                    } else {
                        i = (int) new MySQL(Database.host, Database.port, Database.database, Database.username,
                                Database.password).getReportedTimes(Database.playertable, args[0]);
                    }
                } else if (Bsqlite) {
                    SQLite m = new SQLite();
                    if (m.isDataExists(Database.playertable, "Player", Bukkit.getPlayer(args[0]).getUniqueId().toString())) {
                        i = (int) m.getReportedTimes(Database.playertable, Bukkit.getPlayer(args[0]).getUniqueId());
                    } else {
                        i = (int) m.getReportedTimes(Database.playertable, args[0]);
                    }
                }
            }
            for (Player admin : Bukkit.getOnlinePlayers()) {
                if (admin.hasPermission("reporting.receive") || admin.hasPermission("reporting.*")) {
                    if (i >= plugin.suspect){
                        admin.sendMessage(Config.chatmsgalert.replace("{times}", String.valueOf(i)));
                    }
                }
            }
            Database.saveReport(p, Bukkit.getPlayer(args[0]), reason.toString());
        return false;
    }
}
