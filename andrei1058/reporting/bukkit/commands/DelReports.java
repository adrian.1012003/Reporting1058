package com.andrei1058.reporting.bukkit.commands;

import com.andrei1058.reporting.bukkit.misc.MySQL;
import com.andrei1058.reporting.bukkit.misc.SQLite;
import com.andrei1058.reporting.bukkit.settings.Config;
import com.andrei1058.reporting.bukkit.settings.Database;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import static com.andrei1058.reporting.Main.*;

public class DelReports implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender s, Command c, String stt, String[] args) {
        if (c.getName().equalsIgnoreCase("delreports"))
        if (!(plugin.mysql || Bsqlite)) {
            s.sendMessage(Config.not_available);
            return true;
        }
        if (args.length == 0 || args.length > 1){
            s.sendMessage(Config.del_reports_usage_player);
            return true;
        }
        if (ReportsList.isInt(args[0])){
            s.sendMessage(Config.del_reports_usage_player);
            return true;
        }
        if (!(s.hasPermission("reporting.deleteallplayer")|| s.hasPermission("reporting.*"))){
            s.sendMessage(Config.permission);
            return true;
        }
        if (mysql) {
            MySQL m = new MySQL(Database.host, Database.port, Database.database, Database.username, Database.password);
            if (Bukkit.getPlayer(args[0]) != null && Bukkit.getPlayer(args[0]).getUniqueId() != null && m.isDataExists(Database.maintable, "Reported", Bukkit.getPlayer(args[0]).getUniqueId().toString())) {
                m.deleteData(Database.maintable, "Reported", "=", Bukkit.getPlayer(args[0]).getUniqueId().toString());
                m.deleteData(Database.playertable, "Player", "=", Bukkit.getPlayer(args[0]).getUniqueId().toString());
                s.sendMessage(Config.reports_deleted_player.replace("{player}", args[0]));
            } else if (m.isDataExists(Database.maintable, "ReportedName", args[0])) {
                m.deleteData(Database.maintable, "ReportedName", "=", args[0]);
                m.deleteData(Database.playertable, "PlayerName", "=", args[0]);
                s.sendMessage(Config.reports_deleted_player.replace("{player}", args[0]));
            } else {
                s.sendMessage(Config.no_data_found_player_del_reports);
            }
            m.close();
        } else {
            SQLite m = new SQLite();
            if (Bukkit.getPlayer(args[0]) != null && Bukkit.getPlayer(args[0]).getUniqueId() != null && m.isDataExists(Database.maintable, "Reported", Bukkit.getPlayer(args[0]).getUniqueId().toString())) {
                m.deleteData(Database.maintable, "Reported", "=", Bukkit.getPlayer(args[0]).getUniqueId().toString());
                m.deleteData(Database.playertable, "Player", "=", Bukkit.getPlayer(args[0]).getUniqueId().toString());
                s.sendMessage(Config.reports_deleted_player.replace("{player}", args[0]));
            } else if (m.isDataExists(Database.maintable, "ReportedName", args[0])) {
                m.deleteData(Database.maintable, "ReportedName", "=", args[0]);
                m.deleteData(Database.playertable, "PlayerName", "=", args[0]);
                s.sendMessage(Config.reports_deleted_player.replace("{player}", args[0]));
            } else {
                s.sendMessage(Config.no_data_found_player_del_reports);
            }
        }
        return true;
    }
}
