package com.andrei1058.reporting.bukkit.commands;

import com.andrei1058.reporting.bukkit.misc.MySQL;
import com.andrei1058.reporting.bukkit.misc.SQLite;
import com.andrei1058.reporting.bukkit.settings.Config;
import com.andrei1058.reporting.bukkit.settings.Database;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.UUID;

import static com.andrei1058.reporting.Main.Bsqlite;
import static com.andrei1058.reporting.Main.mysql;

public class CloseReports implements CommandExecutor {
    private HashMap<UUID, Long> confirm = new HashMap<>();

    @Override
    public boolean onCommand(CommandSender s, Command c, String stt, String[] args) {
        if (c.getName().equalsIgnoreCase("closereports"))
        if (args.length != 0){
            s.sendMessage(Config.closereports_usage);
            return true;
        }
        if (!(mysql || Bsqlite)) {
            s.sendMessage(Config.not_available);
            return true;
        }
        if (!(s.hasPermission("reporting.closeall")|| s.hasPermission("reporting.*"))) {
            s.sendMessage(Config.permission);
            return true;
        }
        if (s instanceof ConsoleCommandSender) return true;
        Player p = (Player) s;
        if (!confirm.containsKey(p.getUniqueId())){
            confirm.put(p.getUniqueId(), System.currentTimeMillis());
            s.sendMessage(Config.close_all_confirm);
            return true;
        } else {
            if (System.currentTimeMillis() - 5000 < confirm.get(((Player) s).getUniqueId())){
                if (mysql) {
                    MySQL m = new MySQL(Database.host, Database.port, Database.database, Database.username, Database.password);
                    m.closeReports((Player) s);
                } else {
                    new SQLite().closeReports((Player) s);
                }
                s.sendMessage(Config.reports_closed);
                confirm.remove(p.getUniqueId());
                return true;
            }
            confirm.put(p.getUniqueId(), System.currentTimeMillis());
            s.sendMessage(Config.close_all_confirm);
        }
        return true;
    }
}
