package com.andrei1058.reporting.bukkit.commands;

import com.andrei1058.reporting.bukkit.misc.MySQL;
import com.andrei1058.reporting.bukkit.misc.SQLite;
import com.andrei1058.reporting.bukkit.settings.Config;
import com.andrei1058.reporting.bukkit.settings.Database;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.UUID;

import static com.andrei1058.reporting.Main.*;

public class ReportsList implements CommandExecutor {
    
    @Override
    public boolean onCommand(CommandSender s, Command c, String stt, String[] args) {
        if (c.getName().equalsIgnoreCase("reportslist"))
        if (!(plugin.mysql || Bsqlite)) {
            s.sendMessage(Config.not_available);
            return true;
        }
        if (!(s.hasPermission("reporting.list")|| s.hasPermission("reporting.*"))) {
            s.sendMessage((Config.permission));
            return true;
        }
        if (args.length <= 2) {
            if (args.length == 0 || (isInt(args[0]) && args.length == 1)) {
                int start = 0;
                int limit = r_p_p;
                if (args.length == 1) {
                    if (isInt(args[0])) {
                        limit = r_p_p * Integer.parseInt(args[0])+1;
                        start = r_p_p * (Integer.parseInt(args[0])-1);
                    } else {
                        s.sendMessage((Config.reports_usage));
                    }
                }
                s.sendMessage((Config.loading));
                ArrayList list = new ArrayList();
                list.add("ID");
                list.add("Reporter");
                list.add("Reported");
                list.add("Reason");
                list.add("World");
                list.add("Date");
                list.add("ReporterName");
                list.add("ReportedName");
                list.add("Status");
                ArrayList<ArrayList> lista = new ArrayList<>();
                if (mysql) {
                    MySQL m = new MySQL(Database.host, Database.port, Database.database, Database.username, Database.password);
                    lista = m.getData(Database.maintable, list, String.valueOf(start), String.valueOf(limit));
                    m.close();
                } else if (Bsqlite){
                    lista = new SQLite().getData(Database.maintable, list, String.valueOf(start), String.valueOf(limit));
                }
                if (lista.isEmpty()){
                    s.sendMessage((Config.no_data_found));
                    return true;
                }
                for (Object st : lista) {
                    ArrayList a = (ArrayList) st;
                    String id = (String) a.get(0);
                    String reporter;
                    String reported;
                    String reason;
                    String server = (String) a.get(4);
                    String date = (String) a.get(5);
                    String status;
                    if (a.get(8).equals("1")) {
                        status = Config.status_active;
                    } else {
                        status = Config.status_closed;
                    }
                    if (Bukkit.getPlayer(UUID.fromString((String) a.get(1))) != null) {
                        reporter = Bukkit.getPlayer(UUID.fromString((String) a.get(1))).getName();
                    } else {
                        reporter = (String) a.get(6);
                    }
                    if (Bukkit.getPlayer(UUID.fromString((String) a.get(2))) != null) {
                        reported = Bukkit.getPlayer(UUID.fromString((String) a.get(2))).getName();
                    } else {
                        reported = (String) a.get(7);
                    }
                    reason = (String) a.get(3);
                    s.sendMessage((Config.report_list_all.replace("{id}", id).replace("{world}", server).replace("{reported}", reported)
                            .replace("{reason}", reason).replace("{reporter}", reporter).replace("{date}", date).replace("{status}", status)));
                }
                if (args.length == 0){
                    s.sendMessage(Config.reports_next_page.replace("{page}", "2"));
                }
            } else {
                int start = 0;
                int limit = r_p_p;
                if (args.length == 2) {
                    if (isInt(args[1])) {
                        limit = r_p_p * Integer.parseInt(args[1])+1;
                        start = r_p_p * (Integer.parseInt(args[1])-1);
                    } else {
                        s.sendMessage((Config.reports_usage));
                    }
                }
                s.sendMessage((Config.loading));
                ArrayList list = new ArrayList();
                list.add("ID");
                list.add("Reporter");
                list.add("Reported");
                list.add("Reason");
                list.add("World");
                list.add("Date");
                list.add("ReporterName");
                list.add("ReportedName");
                list.add("Status");
                ArrayList<ArrayList> lista = new ArrayList<>();
                if (mysql) {
                    MySQL m = new MySQL(Database.host, Database.port, Database.database, Database.username, Database.password);
                    lista = m.getPlayerReports(Database.maintable, list, String.valueOf(start), String.valueOf(limit), args[0], Bukkit.getPlayer(args[0]));
                    m.close();
                } else if (Bsqlite){
                    lista = new SQLite().getPlayerReports(Database.maintable, list, String.valueOf(start), String.valueOf(limit), args[0], Bukkit.getPlayer(args[0]));
                }
                if (lista.isEmpty()){
                    s.sendMessage((Config.no_data_found));
                    return true;
                }
                for (Object st : lista) {
                    ArrayList a = (ArrayList) st;
                    String id = (String) a.get(0);
                    String reporter;
                    String reported;
                    String reason;
                    String server = (String) a.get(4);
                    String date = (String) a.get(5);
                    String status;
                    if (a.get(8).equals("1")){
                        status = Config.status_active;
                    } else {
                        status = Config.status_closed;
                    }
                    if (Bukkit.getPlayer(UUID.fromString((String) a.get(1))) != null) {
                        reporter = Bukkit.getPlayer(UUID.fromString((String) a.get(1))).getName();
                    } else {
                        reporter = (String) a.get(6);
                    }
                    if (Bukkit.getPlayer(UUID.fromString((String) a.get(2))) != null) {
                        reported = Bukkit.getPlayer(UUID.fromString((String) a.get(2))).getName();
                    } else {
                        reported = (String) a.get(7);
                    }
                    reason = (String) a.get(3);
                    s.sendMessage((Config.report_list_all.replace("{id}", id).replace("{world}", server).replace("{reported}", reported)
                            .replace("{reason}", reason).replace("{reporter}", reporter).replace("{date}", date).replace("{status}", status)));
                }
                if (args.length < 0){
                    s.sendMessage(Config.reports_player_next_page.replace("{page}", "2").replace("{username}", args[0]));
                }
            }
        } else {
            s.sendMessage((Config.reports_usage));
        }
        return true;
    }

    public static boolean isInt(String x) {
        try {
            Integer.parseInt(x);
            return true;
        } catch (NumberFormatException e) {
        }
        return false;
    }
}
