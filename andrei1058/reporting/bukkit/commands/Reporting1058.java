package com.andrei1058.reporting.bukkit.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import static com.andrei1058.reporting.Main.plugin;

public class Reporting1058 implements CommandExecutor{

    @Override
    public boolean onCommand(CommandSender s, Command c, String st, String[] args) {
        if (c.getName().equalsIgnoreCase("reporting1058"))
        s.sendMessage("");
        s.sendMessage("§9Reporting1058 §8- §f"+plugin.getDescription().getVersion());
        s.sendMessage("");
        s.sendMessage("§7/report <player> <reason> §e- report a player");
        s.sendMessage("§7/delreport <id> §e- delete a report");
        s.sendMessage("§7/delreports <player> §e- delete a player reports");
        s.sendMessage("§7/reports <page> §e- view the reports list");
        s.sendMessage("§7/reports <player> <page> §e- view a player reports");
        s.sendMessage("§7/reportinfo <id> §e- view info");
        s.sendMessage("§7/areports <page> §e- view the active reports");
        s.sendMessage("§7/mostreported <page> §e- view the most reported p");
        s.sendMessage("§7/closereport <id> <verdict> §e- close a report");
        s.sendMessage("§7/closereports §e- close all reports");
        return true;
    }
}
