package com.andrei1058.reporting.bukkit.commands;

import com.andrei1058.reporting.bukkit.misc.MySQL;
import com.andrei1058.reporting.bukkit.misc.SQLite;
import com.andrei1058.reporting.bukkit.settings.Config;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.UUID;

import static com.andrei1058.reporting.Main.Bsqlite;
import static com.andrei1058.reporting.Main.mysql;
import static com.andrei1058.reporting.bukkit.settings.Database.*;

public class ReportInfo implements CommandExecutor{

    @Override
    public boolean onCommand(CommandSender s, Command c, String stt, String[] args) {
        if (c.getName().equalsIgnoreCase("reportinfo"))
            if (args.length != 1) {
                s.sendMessage(Config.repinfo_usage);
                return true;
            }
        if (!(mysql || Bsqlite)) {
            s.sendMessage(Config.not_available);
            return true;
        }
        if (!(s.hasPermission("reporting.info") || s.hasPermission("reporting.*"))) {
            s.sendMessage(Config.permission);
            return true;
        }
        if (mysql) {
            MySQL m = new MySQL(host, port, database, username, password);
            if (!m.isDataExists(maintable, "ID", args[0])) {
                s.sendMessage(Config.rep_not_found);
                m.close();
                return true;
            }
            s.sendMessage(Config.loading);
            ArrayList<String> report = m.getReport("ID", args[0]);
            int r_times;
            String reported;
            String reporter;
            String status;
            if (report.get(8).equals("1")) {
                status = Config.status_active;
            } else {
                status = Config.status_closed;
            }
            if (Bukkit.getPlayer(UUID.fromString(report.get(2))) != null) {
                reported = Bukkit.getPlayer(UUID.fromString(report.get(2))).getName();
                r_times = (int) m.getReportedTimes(playertable, UUID.fromString(report.get(2)));
            } else {
                reported = report.get(7);
                r_times = (int) m.getReportedTimes(playertable, report.get(7));
            }
            if (Bukkit.getPlayer(UUID.fromString(report.get(1))) != null) {
                reporter = Bukkit.getPlayer(UUID.fromString(report.get(1))).getName();
            } else {
                reporter = report.get(6);
            }
            m.close();
            if (Integer.parseInt(report.get(8)) == 1) {
                for (String msg : Config.report_info) {
                    s.sendMessage(msg.replace("{id}", String.valueOf(report.get(0))).replace("{reporter}", reporter).replace("{reported}", reported)
                            .replace("{reason}", report.get(3)).replace("{world}", report.get(4)).replace("{date}", report.get(5)).replace("{times}", String.valueOf(r_times)).replace("{status}", status));
                }
            } else {
                String moderator;
                if (Bukkit.getPlayer(report.get(10)) != null) {
                    moderator = Bukkit.getPlayer(report.get(10)).getName();
                } else {
                    moderator = report.get(11);
                }
                for (String msg : Config.report_info_if_closed) {
                    s.sendMessage(msg.replace("{id}", String.valueOf(report.get(0))).replace("{reporter}", reporter).replace("{reported}", reported)
                            .replace("{reason}", report.get(3)).replace("{world}", report.get(4)).replace("{date}", report.get(5)).replace("{times}", String.valueOf(r_times)).replace("{status}", status)
                            .replace("{moderator}", moderator).replace("{verdict}", report.get(9)).replace("{close_date}", report.get(12)));
                }
            }
        } else {
            SQLite m = new SQLite();
            if (!m.isDataExists(maintable, "ID", args[0])) {
                s.sendMessage(Config.rep_not_found);
                return true;
            }
            s.sendMessage(Config.loading);
            ArrayList<String> report = m.getReport("ID", args[0]);
            int r_times;
            String reported;
            String reporter;
            String status;
            if (report.get(8).equals("1")) {
                status = Config.status_active;
            } else {
                status = Config.status_closed;
            }
            if (Bukkit.getPlayer(UUID.fromString(report.get(2))) != null) {
                reported = Bukkit.getPlayer(UUID.fromString(report.get(2))).getName();
                r_times = (int) m.getReportedTimes(playertable, UUID.fromString(report.get(2)));
            } else {
                reported = report.get(7);
                r_times = (int) m.getReportedTimes(playertable, report.get(7));
            }
            if (Bukkit.getPlayer(UUID.fromString(report.get(1))) != null) {
                reporter = Bukkit.getPlayer(UUID.fromString(report.get(1))).getName();
            } else {
                reporter = report.get(6);
            }
            if (Integer.parseInt(report.get(8)) == 1) {
                for (String msg : Config.report_info) {
                    s.sendMessage(msg.replace("{id}", String.valueOf(report.get(0))).replace("{reporter}", reporter).replace("{reported}", reported)
                            .replace("{reason}", report.get(3)).replace("{world}", report.get(4)).replace("{date}", report.get(5)).replace("{times}", String.valueOf(r_times)).replace("{status}", status));
                }
            } else {
                String moderator;
                if (Bukkit.getPlayer(report.get(10)) != null) {
                    moderator = Bukkit.getPlayer(report.get(10)).getName();
                } else {
                    moderator = report.get(11);
                }
                for (String msg : Config.report_info_if_closed) {
                    s.sendMessage(msg.replace("{id}", String.valueOf(report.get(0))).replace("{reporter}", reporter).replace("{reported}", reported)
                            .replace("{reason}", report.get(3)).replace("{world}", report.get(4)).replace("{date}", report.get(5)).replace("{times}", String.valueOf(r_times)).replace("{status}", status)
                            .replace("{moderator}", moderator).replace("{verdict}", report.get(9)).replace("{close_date}", report.get(12)));
                }
            }
        }
        return true;
    }
}
