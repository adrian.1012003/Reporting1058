package com.andrei1058.reporting.bukkit.commands;

import com.andrei1058.reporting.bukkit.misc.MySQL;
import com.andrei1058.reporting.bukkit.misc.SQLite;
import com.andrei1058.reporting.bukkit.settings.Config;
import com.andrei1058.reporting.bukkit.settings.Database;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import static com.andrei1058.reporting.Main.*;
import static com.andrei1058.reporting.bukkit.commands.ReportsList.isInt;

public class CloseReport implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender s, Command c, String stt, String[] args) {
        if (c.getName().equalsIgnoreCase("closereport"))
        if (!(plugin.mysql || Bsqlite)) {
            s.sendMessage(Config.not_available);
            return true;
        }
        if (args.length < 2){
            s.sendMessage(Config.close_rep_usage);
            return true;
        }
        if (!isInt(args[0])){
            s.sendMessage(Config.close_rep_usage);
            return true;
        }
        if (!(s.hasPermission("reporting.close")|| s.hasPermission("reporting.*"))){
            s.sendMessage(Config.permission);
            return true;
        }
        StringBuilder verdict = new StringBuilder();
        for (int i = 1; i < args.length; i++) {
            verdict.append(args[i] + " ").toString();
        }
        if (mysql) {
            MySQL m = new MySQL(Database.host, Database.port, Database.database, Database.username, Database.password);
            if (!m.isDataExists(Database.maintable, "ID", args[0])) {
                s.sendMessage(Config.rep_not_found);
                m.close();
                return true;
            }
            if (m.isClosed(args[0])) {
                s.sendMessage(Config.already_closed);
                m.close();
                return true;
            }
            m.closeReport(args[0], String.valueOf(verdict).replace("'", "''"), (Player) s);
        } else {
            SQLite m = new SQLite();
            if (!m.isDataExists(Database.maintable, "ID", args[0])) {
                s.sendMessage(Config.rep_not_found);
                return true;
            }
            if (m.isClosed(args[0])) {
                s.sendMessage(Config.already_closed);
                return true;
            }
            m.closeReport(args[0], String.valueOf(verdict).replace("'", "''"), (Player) s);
        }
        s.sendMessage(Config.rep_closed.replace("{id}",args[0]));
        return true;
    }
}
