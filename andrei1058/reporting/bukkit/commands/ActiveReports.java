package com.andrei1058.reporting.bukkit.commands;

import com.andrei1058.reporting.bukkit.misc.MySQL;
import com.andrei1058.reporting.bukkit.misc.SQLite;
import com.andrei1058.reporting.bukkit.settings.Config;
import com.andrei1058.reporting.bukkit.settings.Database;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.UUID;

import static com.andrei1058.reporting.Main.*;
import static com.andrei1058.reporting.bukkit.commands.ReportsList.isInt;

public class ActiveReports implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender s, Command c, String stt, String[] args) {
        if (c.getName().equalsIgnoreCase("activereports"))
        if (!(mysql || Bsqlite)) {
            s.sendMessage(Config.not_available);
            return true;
        }
        if (!(s.hasPermission("reporting.list")|| s.hasPermission("reporting.*"))){
            s.sendMessage(Config.permission);
            return true;
        }
        if (args.length <= 1) {
            int start = 0;
            int limit = r_p_p;
            if (args.length == 1) {
                if (isInt(args[0])) {
                    limit = r_p_p * Integer.parseInt(args[0])+1;
                    start = r_p_p * (Integer.parseInt(args[0])-1);
                } else {
                    s.sendMessage(Config.active_reports_usage);
                }
            }
            s.sendMessage(Config.loading);
            ArrayList list = new ArrayList();
            list.add("ID");
            list.add("Reporter");
            list.add("Reported");
            list.add("Reason");
            list.add("World");
            list.add("Date");
            list.add("ReporterName");
            list.add("ReportedName");
            list.add("Status");
            ArrayList<ArrayList> lista;
            if (mysql) {
                MySQL m = new MySQL(Database.host, Database.port, Database.database, Database.username, Database.password);
                lista = m.getActiveReports(Database.maintable, list, String.valueOf(start), String.valueOf(limit));
            } else {
                lista = new SQLite().getActiveReports(Database.maintable, list, String.valueOf(start), String.valueOf(limit));
            }
            if (lista.size() == 0){
                s.sendMessage(Config.no_active_reports);
                return true;
            }
            for (Object st : lista) {
                ArrayList a = (ArrayList) st;
                String id = (String) a.get(0);
                String reporter;
                String reported;
                String reason;
                String world = (String) a.get(4);
                String date = (String) a.get(5);
                String status;
                if (a.get(8).equals("2")){
                    status = Config.status_active;
                } else {
                    status = Config.status_closed;
                }
                if (Bukkit.getPlayer(UUID.fromString((String) a.get(1))) != null) {
                    reporter = Bukkit.getPlayer(UUID.fromString((String) a.get(1))).getName();
                } else {
                    reporter = (String) a.get(6);
                }
                if (Bukkit.getPlayer(UUID.fromString((String) a.get(2))) != null) {
                    reported = Bukkit.getPlayer(UUID.fromString((String) a.get(2))).getName();
                } else {
                    reported = (String) a.get(7);
                }
                reason = (String) a.get(3);
                s.sendMessage(Config.report_list_active.replace("{id}", id).replace("{world}", world).replace("{reported}", reported)
                        .replace("{reason}", reason).replace("{reporter}", reporter).replace("{date}", date).replace("{status}", status));
            }
            if (args.length == 0){
                s.sendMessage(Config.areports_next_page.replace("{page}", "2"));
            } else {
                s.sendMessage(Config.areports_next_page.replace("{page}", String.valueOf(Integer.parseInt(args[0])+1)));
            }
        } else {
            s.sendMessage(Config.active_reports_usage);
        }
        return false;
    }
}
