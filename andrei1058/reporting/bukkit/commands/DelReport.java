package com.andrei1058.reporting.bukkit.commands;

import com.andrei1058.reporting.bukkit.misc.MySQL;
import com.andrei1058.reporting.bukkit.misc.SQLite;
import com.andrei1058.reporting.bukkit.settings.Config;
import com.andrei1058.reporting.bukkit.settings.Database;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import static com.andrei1058.reporting.Main.*;

public class DelReport implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender s, Command c, String stt,  String[] args) {
        if (c.getName().equalsIgnoreCase("delreport"))
        if (!(plugin.mysql || Bsqlite)) {
            s.sendMessage(Config.not_available);
            return true;
        }
        if (args.length == 0 || args.length > 1){
            s.sendMessage(Config.delrep_usage);
            return true;
        }
        if (!ReportsList.isInt(args[0])){
            s.sendMessage(Config.delrep_usage);
            return true;
        }
        if (!(s.hasPermission("reporting.delete")|| s.hasPermission("reporting.*"))){
            s.sendMessage(Config.permission);
            return true;
        }
        if (mysql) {
            MySQL m = new MySQL(Database.host, Database.port, Database.database, Database.username, Database.password);
            if (!m.isDataExists(Database.maintable, "ID", args[0])) {
                s.sendMessage(Config.rep_not_found);
                m.close();
                return true;
            }
            m.deleteData(Database.maintable, "ID", "=", args[0]);

            m.close();
        } else {
            SQLite m = new SQLite();
            if (!m.isDataExists(Database.maintable, "ID", args[0])) {
                s.sendMessage(Config.rep_not_found);
                return true;
            }
            m.deleteData(Database.maintable, "ID", "=", args[0]);
        }
        s.sendMessage(Config.rep_deleted);
        return true;
    }
}
