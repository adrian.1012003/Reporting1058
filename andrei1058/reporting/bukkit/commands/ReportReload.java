package com.andrei1058.reporting.bukkit.commands;


import com.andrei1058.reporting.bukkit.settings.Config;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import static com.andrei1058.reporting.bukkit.settings.Config.setupConfig;

/**
 * Copyright Andrei Dascalu - andrei1058 @spigotmc.org
 * Reporting1058 class written on 07/04/2017
 */
public class ReportReload implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender s, Command c, String st, String[] args) {
        if (c.getName().equalsIgnoreCase("rreload")) {
            if (s instanceof Player) {
                Player p = (Player) s;
                if (!p.isOp()) {
                    if (!(p.hasPermission("reporting.reload") || p.hasPermission("reporting.*"))) {
                        p.sendMessage(Config.permission);
                        return true;
                    }
                }
            }
            setupConfig();
            s.sendMessage("§9Reporting §8» §aConfig reloaded!");
            return false;
        }
        return false;
    }
}
