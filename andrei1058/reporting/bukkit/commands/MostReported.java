package com.andrei1058.reporting.bukkit.commands;

import com.andrei1058.reporting.bukkit.misc.MySQL;
import com.andrei1058.reporting.bukkit.misc.SQLite;
import com.andrei1058.reporting.bukkit.settings.Config;
import com.andrei1058.reporting.bukkit.settings.Database;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.UUID;

import static com.andrei1058.reporting.Main.*;
import static com.andrei1058.reporting.bukkit.commands.ReportsList.isInt;

public class MostReported implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender s, Command c, String stt, String[] args) {
        if (c.getName().equalsIgnoreCase("mostreported"))
        if (!(s.hasPermission("reporting.list")|| s.hasPermission("reporting.*"))) {
            s.sendMessage(Config.permission);
            return true;
        }
        if (!(mysql || Bsqlite)) {
            s.sendMessage(Config.not_available);
            return true;
        }
        if (args.length <= 1) {
            int start = 0;
            int limit = r_p_p;
            if (args.length == 1) {
                if (isInt(args[0])) {
                    limit = r_p_p * Integer.parseInt(args[0]) + 1;
                    start = r_p_p * (Integer.parseInt(args[0]) - 1);
                } else {
                    s.sendMessage(Config.mostreported_usage);
                }
            }
            s.sendMessage(Config.loading);
            ArrayList list = new ArrayList();
            list.add("Player");
            list.add("PlayerName");
            list.add("Times");
            ArrayList<ArrayList> lista;
            if (mysql) {
                MySQL m = new MySQL(Database.host, Database.port, Database.database, Database.username, Database.password);
                lista = m.getTopReports(Database.playertable, list, String.valueOf(start), String.valueOf(limit));
            } else {
                lista = new SQLite().getTopReports(Database.playertable, list, String.valueOf(start), String.valueOf(limit));
            }
            if (lista.size() == 0) {
                s.sendMessage(Config.no_active_reports);
                return true;
            }
            for (Object st : lista) {
                ArrayList a = (ArrayList) st;
                String player;
                if (Bukkit.getPlayer(UUID.fromString(String.valueOf(a.get(0)))) != null) {
                    player = Bukkit.getPlayer(UUID.fromString(String.valueOf(a.get(0)))).getName();
                } else {
                    player = (String) a.get(1);
                }
                String reported = (String) a.get(2);
                start++;
                s.sendMessage(Config.mostreported_list.replace("{username}", player).replace("{nr}", String.valueOf(start)).replace("{reports}", reported));
            }
        }
        return false;
    }
}
