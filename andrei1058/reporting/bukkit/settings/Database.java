package com.andrei1058.reporting.bukkit.settings;

import com.andrei1058.reporting.bukkit.misc.MySQL;
import com.andrei1058.reporting.bukkit.misc.SQLite;
import org.bukkit.entity.Player;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static com.andrei1058.reporting.Main.Bsqlite;
import static com.andrei1058.reporting.Main.mysql;

public class Database {
    public static String host = null;
    public static int port = 0;
    public static String database = null;
    public static String username = null;
    public static String password = null;
    public static String maintable = null;
    public static String playertable = null;
    public static SimpleDateFormat date = new SimpleDateFormat(Config.dateformat);

    public static void setup() {
        if (mysql) {
            ArrayList<String> records = new ArrayList<>();
            records.add("ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY");
            records.add("Reporter char(36)");
            records.add("Reported char(36)");
            records.add("Reason char(50)");
            records.add("World char(36)");
            records.add("Date char(36)");
            records.add("ReporterName char(36)");
            records.add("ReportedName char(36)");
            records.add("Status char(36)");
            records.add("Verdict char(36)");
            records.add("Moderator char(36)");
            records.add("ModeratorName char(36)");
            records.add("ClosedOn char(36)");
            MySQL m = new MySQL(host, port, database, username, password);
            m.createTable(maintable, records);

            ArrayList<String> records2 = new ArrayList<>();
            records2.add("Player char(36)");
            records2.add("PlayerName char(36)");
            records2.add("Times INT");
            MySQL m2 = new MySQL(host, port, database, username, password);
            m2.createTable(playertable, records2);
        } else if (Bsqlite) {
            ArrayList<String> records = new ArrayList<>();
            records.add("ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL");
            records.add("Reporter char(36)");
            records.add("Reported char(36)");
            records.add("Reason char(50)");
            records.add("World char(36)");
            records.add("Date char(36)");
            records.add("ReporterName char(36)");
            records.add("ReportedName char(36)");
            records.add("Status char(36)");
            records.add("Verdict char(36)");
            records.add("Moderator char(36)");
            records.add("ModeratorName char(36)");
            records.add("ClosedOn char(36)");
            new SQLite().createTable(maintable, records);

            ArrayList<String> records2 = new ArrayList<>();
            records2.add("Player char(36)");
            records2.add("PlayerName char(36)");
            records2.add("Times INT");
            new SQLite().createTable(playertable, records2);
        }
    }

    public static void saveReport(Player Reporter, Player Reported, String Reason){
        if (mysql){
            MySQL m = new MySQL(host, port, database, username, password);
            m.connect();
            m.addReportAsNumber(playertable, Reported.getName(), Reported);
            ArrayList main1 = new ArrayList();
            ArrayList main2 = new ArrayList();
            main1.add("Reporter");
            main1.add("Reported");
            main1.add("Reason");
            main1.add("World");
            main1.add("Date");
            main1.add("ReporterName");
            main1.add("ReportedName");
            main1.add("Status");
            main2.add(Reporter.getUniqueId().toString());
            main2.add(Reported.getUniqueId().toString());
            main2.add(Reason.replace("'", "''"));
            main2.add(Reporter.getWorld().getName());
            main2.add(date.format(new Date()));
            main2.add(Reporter.getName());
            main2.add(Reported.getName());
            main2.add("1");
            m.createDate(maintable, main1, main2);
            m.close();
        } else if (Bsqlite){
            new SQLite().addReportAsNumber(playertable, Reported.getName(), Reported);
            ArrayList main1 = new ArrayList();
            ArrayList main2 = new ArrayList();
            main1.add("Reporter");
            main1.add("Reported");
            main1.add("Reason");
            main1.add("World");
            main1.add("Date");
            main1.add("ReporterName");
            main1.add("ReportedName");
            main1.add("Status");
            main2.add(Reporter.getUniqueId().toString());
            main2.add(Reported.getUniqueId().toString());
            main2.add(Reason.replace("'", "''"));
            main2.add(Reporter.getWorld().getName());
            main2.add(date.format(new Date()));
            main2.add(Reporter.getName());
            main2.add(Reported.getName());
            main2.add("1");
            new SQLite().createDate(maintable, main1, main2);
        }
    }
}
