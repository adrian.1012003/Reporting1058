package com.andrei1058.reporting.bukkit.settings;

import org.bukkit.ChatColor;
import org.bukkit.configuration.Configuration;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.stream.Collectors;

import static com.andrei1058.reporting.Main.*;
import static com.andrei1058.reporting.bukkit.settings.Database.*;

public class Config {

    private static File file = new File(plugin.getDataFolder().getPath(), "config.yml");
    public static String insuficientargs = "&9Report> &7Usage: /report [user] [reason]";
    public static String offlinereported = "&9Report> &7{player} is not online!";
    public static String reportsent = "&9Report> &7Your report has been sent to all online staff!";
    public static String titletitle = "&4New Report";
    public static String titlesubtitle = "&a{reporter} &c-> &4{reported}";
    public static String actionmsg = "&c{reported} &awas reported for: &e{reason}";
    public static String chatmsg = "&9Report> &c{reported} &7was reported by &a{reporter} &7for &e{reason}";
    public static String chatmsgalert = "&9Report> &7This player was reported for &c{times} &7times!";
    public static String reporturself = "&9Report> &7You can't report yourself!";
    public static String dateformat = "dd/MM/yyyy, HH:mm";
    public static String permission = "&9Report> &7You can't do this!";
    public static String report_list_all = "&9#{id} &a{world} &e{reported} &f- {reason} &7[{status}&7]";
    public static String report_list_active = "&9#{id} &a{world} &e{reported} &f- {reason}";
    public static String rep_not_found = "&9Report> &cId not found!";
    public static String status_active = "&cACTIVE";
    public static String status_closed = "&8CLOSED";
    public static String rep_deleted = "&9Report> &aReport deleted!";
    public static String repinfo_usage = "&9Report> &7Usage: /rinfo <id>";
    public static String not_available = "&9Report> &7Not available";
    public static String loading = "&9Report> &7Loading. . .";
    public static String report_cooldown = "&9Report> &7You have to wait a bit before reporting again!";
    public static String close_rep_usage = "&9Report> &7Usage: /closereport <id> <verdict>";
    public static String rep_closed = "&9Report> &7Report {id} is now closed.";
    public static String reports_usage = "&9Report> &7Usage: /reports <page> or /reports <player> <page>";
    public static String active_reports_usage = "&9Report> &7Usage: /activereports <page>";
    public static String no_active_reports = "&9Report> &7There isn't any active report!";
    public static String closereports_usage = "&9Report> &7Usage: /closereports";
    public static String reports_closed = "&9Report> &7The reports are now all closed!";
    public static String close_all_confirm = "&9Report> &7Type again &8/closereports &7to confirm.";
    public static String current_server_active_reports = "&9Report> &7There are &c{reports} &7active report(s) on this server.";
    public static String disabled_world = "&9Report> &7You can't use this command on this server!";
    public static ArrayList<String> report_info = new ArrayList<>();
    public static ArrayList<String> report_info_if_closed = new ArrayList<>();
    public static String delrep_usage = "&cUsage: /delreport <id>";
    public static String reports_deleted_player = "&9Report> &7All reports for {player} were deleted!";
    public static String del_reports_usage_player = "&9Report> &7Usage: /delreports <player>";
    public static String no_data_found_player_del_reports = "&9Reports> &7Player not found!";
    public static String already_closed = "&9Report> &7This report is already closed!";
    public static String no_data_found = "&9Report> &7Nothing found!";
    public static String mostreported_usage = "&9Report> &7/mostreported <page>";
    public static String mostreported_list = "&6&l{nr}. &f{username} - &8&l{reports} &7reports.";
    public static String anti_report = "&9Report> &7You can't report this player!";
    public static String reports_next_page = "&9Report> &7Type &9/reports {page} &7to see more.";
    public static String areports_next_page = "&9Report> &7Type &9/areports {page} &7to see more.";
    public static String reports_player_next_page = "&9Report> &7Type &9/reports {username} {page} &7to see more.";

    public static void setupConfig() {
        report_info.add("&8________________[ &9Report &f#&7{id} &8]________________");
        report_info.add("&9Date: &7{date}");
        report_info.add("&9Status: {status}");
        report_info.add("&9Reported: &7{reported}");
        report_info.add("&9Reason: &7{reason}");
        report_info.add("&9World: &7{world}");
        report_info.add("&9Reporter: &7{reporter}");
        report_info.add("&c&l> &7{reported} &7was reported {times} times.");
        report_info_if_closed.add("&8________________[ &9Report &f#&7{id} &8]________________");
        report_info_if_closed.add("&9Date: &7{date}");
        report_info_if_closed.add("&9Status: {status}");
        report_info_if_closed.add("&9Reported: &7{reported}");
        report_info_if_closed.add("&9Reason: &7{reason}");
        report_info_if_closed.add("&9World: &7{world}");
        report_info_if_closed.add("&9Reporter: &7{reporter}");
        report_info_if_closed.add("&9Verdict: &7{verdict}");
        report_info_if_closed.add("&9Moderator: &7{moderator}");
        report_info_if_closed.add("&9Closed on: &7{close_date}");
        report_info_if_closed.add("&c&l> &7{reported} &7was reported {times} times.");
        ArrayList ds = new ArrayList<String>();
        ds.add("yourmap");
        if (!plugin.getDataFolder().exists())
            plugin.getDataFolder().mkdir();

        if (!file.exists()) {
            try {
                file.createNewFile();
                org.bukkit.configuration.file.YamlConfiguration config = org.bukkit.configuration.file.YamlConfiguration.loadConfiguration(file);
                config.set("allow-titles", true);
                config.set("allow-action", true);
                config.set("allow-motd", true);
                config.set("mysql.use", false);
                config.set("min-reports-suspect", 5);
                config.set("report-cooldown", 30);
                config.set("report-per-page", 7);
                config.set("mysql.host", "localhost");
                config.set("mysql.port", 3306);
                config.set("mysql.database", "reports");
                config.set("mysql.table_prefix", "serverName_");
                config.set("mysql.username", "root");
                config.set("mysql.password", "yourpass");
                config.set("disabled-worlds", ds);
                config.set("msg.insuficientargs", insuficientargs);
                config.set("msg.offline", offlinereported);
                config.set("msg.report-sent", reportsent);
                config.set("msg.title", titletitle);
                config.set("msg.subtitle", titlesubtitle);
                config.set("msg.action", actionmsg);
                config.set("msg.chat", chatmsg);
                config.set("msg.chat-suspect", chatmsgalert);
                config.set("msg.reportfail", reporturself);
                config.set("msg.date-format", dateformat);
                config.set("msg.permission", permission);
                config.set("msg.reports-list-all", report_list_all);
                config.set("msg.reports-list-active", report_list_active);
                config.set("msg.rep-not-found", rep_not_found);
                config.set("msg.status-active", status_active);
                config.set("msg.status-closed", status_closed);
                config.set("msg.rep-deleted", rep_deleted);
                config.set("msg.repinfo-usage", repinfo_usage);
                config.set("msg.not-available", not_available);
                config.set("msg.loading", loading);
                config.set("msg.rep-cooldown", report_cooldown);
                config.set("msg.close-usage", close_rep_usage);
                config.set("msg.rep-closed", rep_closed);
                config.set("msg.reports-usage", reports_usage);
                config.set("msg.areports-usage", active_reports_usage);
                config.set("msg.no-active-reports", no_active_reports);
                config.set("msg.close-all-usage", closereports_usage);
                config.set("msg.reports-closed", reports_closed);
                config.set("msg.close-all-confirm", close_all_confirm);
                config.set("msg.motd", current_server_active_reports);
                config.set("msg.disabled-server", disabled_world);
                config.set("msg.delrep-usage", delrep_usage);
                config.set("msg.player-reports-deleted", reports_deleted_player);
                config.set("msg.del-player-reports-usage", del_reports_usage_player);
                config.set("msg.player-not-found", no_data_found_player_del_reports);
                config.set("msg.already-closed", already_closed);
                config.set("msg.no-data-found", no_data_found);
                config.set("msg.rinfo", report_info);
                config.set("msg.rinfo-closed", report_info_if_closed);
                config.set("msg.reports-next-page", reports_next_page);
                config.set("msg.areports-next-page", areports_next_page);
                config.set("msg.player-reports-next-page", reports_player_next_page);
                config.set("msg.mostreported-usage", mostreported_usage);
                config.set("msg.mostreported-list", mostreported_list);
                config.set("msg.antireport", anti_report);

                config.save(file);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            try {
                org.bukkit.configuration.file.YamlConfiguration c = org.bukkit.configuration.file.YamlConfiguration.loadConfiguration(file);
                cfgCheck(c, "allow-titles", true);
                cfgCheck(c, "allow-action", true);
                cfgCheck(c, "allow-motd", true);
                cfgCheck(c, "mysql.use", false);
                cfgCheck(c, "reports-per-page", 7);
                cfgCheck(c, "mysql.host", "localhost");
                cfgCheck(c, "mysql.port", 3306);
                cfgCheck(c, "mysql.database", "reports");
                cfgCheck(c, "mysql.table_prefix", "serverName_");
                cfgCheck(c, "mysql.username", "root");
                cfgCheck(c, "mysql.password", "password");
                cfgCheck(c, "min-reports-suspect", 8);
                cfgCheck(c, "report-cooldown", 30);
                cfgCheck(c, "disabled-worlds", ds);
                cfgCheck(c, "msg.insuficientargs", insuficientargs);
                cfgCheck(c, "msg.offline", offlinereported);
                cfgCheck(c, "msg.report-sent", reportsent);
                cfgCheck(c, "msg.title", titletitle);
                cfgCheck(c, "msg.subtitle", titlesubtitle);
                cfgCheck(c, "msg.action", actionmsg);
                cfgCheck(c, "msg.chat", chatmsg);
                cfgCheck(c, "msg.chat-suspect", chatmsgalert);
                cfgCheck(c, "msg.reportfail", reporturself);
                cfgCheck(c, "msg.date-format", dateformat);
                cfgCheck(c, "msg.permission", permission);
                cfgCheck(c, "msg.report-list-all", report_list_all);
                cfgCheck(c, "msg.report-list-active", report_list_active);
                cfgCheck(c, "msg.rep-not-found", rep_not_found);
                cfgCheck(c, "msg.status-active", status_active);
                cfgCheck(c, "msg.status-closed", status_closed);
                cfgCheck(c, "msg.rep-deleted", rep_deleted);
                cfgCheck(c, "msg.repinfo-usage", repinfo_usage);
                cfgCheck(c, "msg.not-available", not_available);
                cfgCheck(c, "msg.loading", loading);
                cfgCheck(c, "msg.rep-cooldown", report_cooldown);
                cfgCheck(c, "msg.close-usage", close_rep_usage);
                cfgCheck(c, "msg.rep-closed", rep_closed);
                cfgCheck(c, "msg.reports-usage", reports_usage);
                cfgCheck(c, "msg.areports-usage", active_reports_usage);
                cfgCheck(c, "msg.no-active-reports", no_active_reports);
                cfgCheck(c, "msg.aclose-all-usage", closereports_usage);
                cfgCheck(c, "msg.repoorts-closed", reports_closed);
                cfgCheck(c, "msg.close-all-confirm", close_all_confirm);
                cfgCheck(c, "msg.motd", current_server_active_reports);
                cfgCheck(c, "msg.disabled-server", disabled_world);
                cfgCheck(c, "msg.delrep-usage", delrep_usage);
                cfgCheck(c, "msg.player-reports-deleted", reports_deleted_player);
                cfgCheck(c, "msg.del-player-reports-usage", del_reports_usage_player);
                cfgCheck(c, "msg.player-not-found", no_data_found_player_del_reports);
                cfgCheck(c, "msg.already-closed", already_closed);
                cfgCheck(c, "msg.nod-data-found", no_data_found);
                cfgCheck(c, "msg.rinfo", report_info);
                cfgCheck(c, "msg.rinfo-closed", report_info_if_closed);
                cfgCheck(c, "msg.reports-next-page", reports_next_page);
                cfgCheck(c, "msg.areports-next-page", areports_next_page);
                cfgCheck(c, "msg.player-reports-next-page", reports_player_next_page);
                cfgCheck(c, "msg.mostreported-usage", mostreported_usage);
                cfgCheck(c, "msg.mostreported-list", mostreported_list);
                cfgCheck(c, "msg.antireport", anti_report);
                c.save(file);
            } catch (IOException e) {
            }
        }
        org.bukkit.configuration.file.YamlConfiguration c = org.bukkit.configuration.file.YamlConfiguration.loadConfiguration(file);
        titles = loadBoolean(c, "allow-titles");
        action = loadBoolean(c, "allow-action");
        motd = loadBoolean(c, "allow-motd");
        r_p_p = Integer.valueOf(String.valueOf(cfgLoad(c, "reports-per-page", 7)));
        mysql = loadBoolean(c, "mysql.use");
        host = (String) cfgLoad(c, "mysql.host", host);
        port = Integer.valueOf(String.valueOf(cfgLoad(c, "mysql.port", port)));
        database = (String) cfgLoad(c, "mysql.database", database);
        maintable = cfgLoad(c, "mysql.table_prefix", playertable) + "reports";
        playertable = cfgLoad(c, "mysql.table_prefix", playertable) + "player_reports";
        username = (String) cfgLoad(c, "mysql.username", username);
        password = (String) cfgLoad(c, "mysql.password", password);
        suspect = Integer.parseInt(String.valueOf(cfgLoad(c, "min-reports-suspect", suspect)));
        rep_cooldown = Long.parseLong(String.valueOf(cfgLoad(c, "report-cooldown", rep_cooldown)))*1000;
        disabled_worlds = (ArrayList) cfgLoad(c, "disabled-worlds", ds);
        insuficientargs = (String) cfgLoad(c, "msg.insuficientargs", insuficientargs);
        offlinereported = (String) cfgLoad(c, "msg.offline", offlinereported);
        reportsent = (String) cfgLoad(c, "msg.report-sent", reportsent);
        titletitle = (String) cfgLoad(c, "msg.title", titletitle);
        titlesubtitle = (String) cfgLoad(c, "msg.subtitle", titlesubtitle);
        actionmsg = (String) cfgLoad(c, "msg.action", actionmsg);
        chatmsg = (String) cfgLoad(c, "msg.chat", chatmsg);
        chatmsgalert = (String) cfgLoad(c, "msg.chat-suspect", chatmsgalert);
        reporturself = (String) cfgLoad(c, "msg.reportfail", reporturself);
        dateformat = (String) cfgLoad(c, "msg.date-format", dateformat);
        permission = (String) cfgLoad(c, "msg.permission", permission);
        report_list_all = (String) cfgLoad(c, "msg.report-list-all", report_list_all);
        report_list_active = (String) cfgLoad(c, "msg.report-list-active", report_list_active);
        rep_not_found = (String) cfgLoad(c, "msg.rep-not-found", rep_not_found);
        status_active = (String) cfgLoad(c, "msg.status-active", status_active);
        status_closed = (String) cfgLoad(c, "msg.status-closed", status_closed);
        rep_deleted = (String) cfgLoad(c, "msg.rep-deleted", rep_deleted);
        repinfo_usage = (String) cfgLoad(c, "msg.repinfo-usage", repinfo_usage);
        not_available = (String) cfgLoad(c, "msg.not-available", not_available);
        loading = (String) cfgLoad(c, "msg.loading", loading);
        report_cooldown = (String) cfgLoad(c, "msg.rep-cooldown", report_cooldown);
        close_rep_usage = (String) cfgLoad(c, "msg.close-usage", close_rep_usage);
        rep_closed = (String) cfgLoad(c, "msg.rep-closed", rep_closed);
        reports_usage = (String) cfgLoad(c, "msg.reports-usage", reports_usage);
        active_reports_usage = (String) cfgLoad(c, "msg.areports-usage", active_reports_usage);
        no_active_reports = (String) cfgLoad(c, "msg.no-active-reports", no_active_reports);
        closereports_usage = (String) cfgLoad(c, "msg.aclose-all-usage", closereports_usage);
        reports_closed = (String) cfgLoad(c, "msg.repoorts-closed", reports_closed);
        close_all_confirm = (String) cfgLoad(c, "msg.close-all-confirm", close_all_confirm);
        current_server_active_reports = (String) cfgLoad(c, "msg.motd", current_server_active_reports);
        disabled_world = (String) cfgLoad(c, "msg.disabled-server", disabled_world);
        delrep_usage = (String) cfgLoad(c, "msg.delrep-usage", delrep_usage);
        reports_deleted_player = (String) cfgLoad(c, "msg.player-reports-deleted", reports_deleted_player);
        del_reports_usage_player = (String) cfgLoad(c, "msg.del-player-reports-usage", del_reports_usage_player);
        no_data_found_player_del_reports = (String) cfgLoad(c, "msg.player-not-found", no_data_found_player_del_reports);
        already_closed = (String) cfgLoad(c, "msg.already-closed", already_closed);
        no_data_found = (String) cfgLoad(c, "msg.nod-data-found", no_data_found);
        report_info = (ArrayList<String>) cfgLoad(c, "msg.rinfo", report_info);
        report_info_if_closed = (ArrayList<String>) cfgLoad(c, "msg.rinfo-closed", report_info_if_closed);
        reports_next_page = (String) cfgLoad(c, "msg.reports-next-page", reports_next_page);
        areports_next_page = (String) cfgLoad(c, "msg.areports-next-page", areports_next_page);
        reports_player_next_page = (String) cfgLoad(c, "msg.player-reports-next-page", reports_player_next_page);
        mostreported_list = (String) cfgLoad(c, "msg.mostreported-list", mostreported_list);
        mostreported_usage = (String) cfgLoad(c, "msg.mostreported-usage", mostreported_usage);
        anti_report = (String) cfgLoad(c, "msg.antireport", anti_report);
        setup();
    }

    public static void cfgCheck(Configuration cfg, String entry, Object value) {
        if (cfg.get(entry) == null) {
            cfg.set(entry, value);
        }
    }

    public static Object cfgLoad(Configuration cfg, String entry, Object variabila) {
        if (cfg.get(entry) != null) {
            Object x = cfg.get(entry);
            if (x instanceof Integer) {
                return x;
            } else if (x instanceof String) {
                return ChatColor.translateAlternateColorCodes('&', x.toString());
            } else if (x instanceof ArrayList) {
                return ((ArrayList<String>) x).stream().map(s -> ChatColor.translateAlternateColorCodes('&', s)).collect(Collectors.toCollection(ArrayList::new));
            }
            return entry;
        } else {
            return variabila;
        }
    }

    private static boolean loadBoolean(Configuration csf, String name) {
        if (csf.get(name) != null) {
            return csf.getBoolean(name);
        } else {
            return false;
        }
    }
}
