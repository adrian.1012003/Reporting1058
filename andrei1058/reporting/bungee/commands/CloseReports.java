package com.andrei1058.reporting.bungee.commands;

import com.andrei1058.reporting.bungee.misc.MySQL;
import com.andrei1058.reporting.bungee.misc.SQLite;
import com.andrei1058.reporting.bungee.settings.Configuration;
import com.andrei1058.reporting.bungee.settings.Database;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.command.ConsoleCommandSender;

import java.util.HashMap;
import java.util.UUID;

import static com.andrei1058.reporting.bungee.Main.mysql;
import static com.andrei1058.reporting.bungee.Main.sqlite;

public class CloseReports extends Command {
    private HashMap<UUID, Long> confirm = new HashMap<>();
    public CloseReports() {
        super("closereports");
    }
    @Override
    public void execute(CommandSender s, String[] args) {
        if (args.length != 0){
            s.sendMessage(new TextComponent(Configuration.closereports_usage));
            return;
        }
        if (!(mysql || sqlite)) {
            s.sendMessage(new TextComponent(Configuration.not_available));
            return;
        }
        if (!(s.hasPermission("reporting.closeall")|| s.hasPermission("reporting.*"))) {
            s.sendMessage(new TextComponent(Configuration.permission));
            return;
        }
        if (s instanceof ConsoleCommandSender) return;
        ProxiedPlayer p = (ProxiedPlayer) s;
        if (!confirm.containsKey(p.getUniqueId())){
            confirm.put(p.getUniqueId(), System.currentTimeMillis());
            s.sendMessage(new TextComponent(Configuration.close_all_confirm));
            return;
        } else {
            if (System.currentTimeMillis() - 5000 < confirm.get(((ProxiedPlayer) s).getUniqueId())){
                if (mysql) {
                    MySQL m = new MySQL(Database.host, Database.port, Database.database, Database.username, Database.password);
                    m.closeReports((ProxiedPlayer) s);
                } else {
                    new SQLite().closeReports((ProxiedPlayer) s);
                }
                s.sendMessage(new TextComponent(Configuration.reports_closed));
                confirm.remove(p.getUniqueId());
                return;
            }
            confirm.put(p.getUniqueId(), System.currentTimeMillis());
            s.sendMessage(new TextComponent(Configuration.close_all_confirm));
        }
    }
}
