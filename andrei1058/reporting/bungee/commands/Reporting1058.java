package com.andrei1058.reporting.bungee.commands;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.plugin.Command;

import static com.andrei1058.reporting.bungee.Main.plugin;

public class Reporting1058 extends Command{

    public Reporting1058(String name){
        super(name);
    }

    @Override
    public void execute(CommandSender s, String[] args) {
        s.sendMessage(new TextComponent(""));
        s.sendMessage(new TextComponent("§9Reporting1058 §8- §f"+plugin.getDescription().getVersion()));
        s.sendMessage(new TextComponent(""));
        s.sendMessage(new TextComponent("§7/report <player> <reason> §e- report a player"));
        s.sendMessage(new TextComponent("§7/delreport <id> §e- delete a report"));
        s.sendMessage(new TextComponent("§7/delreports <player> §e- delete a player reports"));
        s.sendMessage(new TextComponent("§7/reports <page> §e- view the reports list"));
        s.sendMessage(new TextComponent("§7/reports <player> <page> §e- view a player reports"));
        s.sendMessage(new TextComponent("§7/reportinfo <id> §e- view info"));
        s.sendMessage(new TextComponent("§7/areports <page> §e- view the active reports"));
        s.sendMessage(new TextComponent("§7/mostreported <page> §e- view the most reported p"));
        s.sendMessage(new TextComponent("§7/closereport <id> <verdict> §e- close a report"));
        s.sendMessage(new TextComponent("§7/closereports §e- close all reports"));
    }
}
