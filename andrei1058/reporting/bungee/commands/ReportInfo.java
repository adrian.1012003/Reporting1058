package com.andrei1058.reporting.bungee.commands;

import com.andrei1058.reporting.bungee.misc.MySQL;
import com.andrei1058.reporting.bungee.misc.SQLite;
import com.andrei1058.reporting.bungee.settings.Configuration;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.plugin.Command;

import java.util.ArrayList;
import java.util.UUID;

import static com.andrei1058.reporting.bungee.Main.*;
import static com.andrei1058.reporting.bungee.settings.Database.*;

public class ReportInfo extends Command{

    public ReportInfo(String name){
        super(name);
    }

    @Override
    public void execute(CommandSender s, String[] args) {
        if (args.length != 1) {
            s.sendMessage(new TextComponent(Configuration.repinfo_usage));
            return;
        }
        if (!(mysql || sqlite)) {
            s.sendMessage(new TextComponent(Configuration.not_available));
            return;
        }
        if (!(s.hasPermission("reporting.info") || s.hasPermission("reporting.*"))) {
            s.sendMessage(new TextComponent(Configuration.permission));
            return;
        }
        if (mysql) {
            MySQL m = new MySQL(host, port, database, username, password);
            if (!m.isDataExists(maintable, "ID", args[0])) {
                s.sendMessage(new TextComponent(Configuration.rep_not_found));
                m.close();
                return;
            }
            s.sendMessage(new TextComponent(Configuration.loading));
            ArrayList<String> report = m.getReport("ID", args[0]);
            int r_times;
            String reported;
            String reporter;
            String status;
            if (report.get(8).equals("1")) {
                status = Configuration.status_active;
            } else {
                status = Configuration.status_closed;
            }
            if (plugin.getProxy().getPlayer(UUID.fromString(report.get(2))) != null) {
                reported = plugin.getProxy().getPlayer(UUID.fromString(report.get(2))).getName();
                r_times = (int) m.getReportedTimes(playertable, UUID.fromString(report.get(2)));
            } else {
                reported = report.get(7);
                r_times = (int) m.getReportedTimes(playertable, report.get(7));
            }
            if (plugin.getProxy().getPlayer(UUID.fromString(report.get(1))) != null) {
                reporter = plugin.getProxy().getPlayer(UUID.fromString(report.get(1))).getName();
            } else {
                reporter = report.get(6);
            }
            m.close();
            if (Integer.parseInt(report.get(8)) == 1) {
                for (String msg : Configuration.report_info) {
                    s.sendMessage(new TextComponent(msg.replace("{id}", String.valueOf(report.get(0))).replace("{reporter}", reporter).replace("{reported}", reported)
                            .replace("{reason}", report.get(3)).replace("{server}", report.get(4)).replace("{date}", report.get(5)).replace("{times}", String.valueOf(r_times)).replace("{status}", status)));
                }
            } else {
                String moderator;
                if (plugin.getProxy().getPlayer(report.get(10)) != null) {
                    moderator = plugin.getProxy().getPlayer(report.get(10)).getName();
                } else {
                    moderator = report.get(11);
                }
                for (String msg : Configuration.report_info_if_closed) {
                    s.sendMessage(new TextComponent(msg.replace("{id}", String.valueOf(report.get(0))).replace("{reporter}", reporter).replace("{reported}", reported)
                            .replace("{reason}", report.get(3)).replace("{server}", report.get(4)).replace("{date}", report.get(5)).replace("{times}", String.valueOf(r_times)).replace("{status}", status)
                            .replace("{moderator}", moderator).replace("{verdict}", report.get(9)).replace("{close_date}", report.get(12))));
                }
            }
        } else {
            SQLite m = new SQLite();
            if (!m.isDataExists(maintable, "ID", args[0])) {
                s.sendMessage(new TextComponent(Configuration.rep_not_found));
                return;
            }
            s.sendMessage(new TextComponent(Configuration.loading));
            ArrayList<String> report = m.getReport("ID", args[0]);
            int r_times;
            String reported;
            String reporter;
            String status;
            if (report.get(8).equals("1")) {
                status = Configuration.status_active;
            } else {
                status = Configuration.status_closed;
            }
            if (plugin.getProxy().getPlayer(UUID.fromString(report.get(2))) != null) {
                reported = plugin.getProxy().getPlayer(UUID.fromString(report.get(2))).getName();
                r_times = (int) m.getReportedTimes(playertable, UUID.fromString(report.get(2)));
            } else {
                reported = report.get(7);
                r_times = (int) m.getReportedTimes(playertable, report.get(7));
            }
            if (plugin.getProxy().getPlayer(UUID.fromString(report.get(1))) != null) {
                reporter = plugin.getProxy().getPlayer(UUID.fromString(report.get(1))).getName();
            } else {
                reporter = report.get(6);
            }
            if (Integer.parseInt(report.get(8)) == 1) {
                for (String msg : Configuration.report_info) {
                    s.sendMessage(new TextComponent(msg.replace("{id}", String.valueOf(report.get(0))).replace("{reporter}", reporter).replace("{reported}", reported)
                            .replace("{reason}", report.get(3)).replace("{server}", report.get(4)).replace("{date}", report.get(5)).replace("{times}", String.valueOf(r_times)).replace("{status}", status)));
                }
            } else {
                String moderator;
                if (plugin.getProxy().getPlayer(report.get(10)) != null) {
                    moderator = plugin.getProxy().getPlayer(report.get(10)).getName();
                } else {
                    moderator = report.get(11);
                }
                for (String msg : Configuration.report_info_if_closed) {
                    s.sendMessage(new TextComponent(msg.replace("{id}", String.valueOf(report.get(0))).replace("{reporter}", reporter).replace("{reported}", reported)
                            .replace("{reason}", report.get(3)).replace("{server}", report.get(4)).replace("{date}", report.get(5)).replace("{times}", String.valueOf(r_times)).replace("{status}", status)
                            .replace("{moderator}", moderator).replace("{verdict}", report.get(9)).replace("{close_date}", report.get(12))));
                }
            }
        }
    }
}
