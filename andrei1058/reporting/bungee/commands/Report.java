package com.andrei1058.reporting.bungee.commands;

import com.andrei1058.reporting.bungee.misc.MySQL;
import com.andrei1058.reporting.bungee.misc.SQLite;
import com.andrei1058.reporting.bungee.misc.Titles;
import com.andrei1058.reporting.bungee.settings.Configuration;
import com.andrei1058.reporting.bungee.settings.Database;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.plugin.TabExecutor;
import net.md_5.bungee.command.ConsoleCommandSender;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

import static com.andrei1058.reporting.bungee.Main.*;

public class Report extends Command implements TabExecutor {

    private static HashMap<UUID, Long> cooldown = new HashMap();

    public Report(){
        super("report");
    }

    @Override
    public void execute(CommandSender s, String[] args) {
        if (s instanceof ConsoleCommandSender) return;
        ProxiedPlayer p = (ProxiedPlayer) s;
        if (args.length <= 1) {
            p.sendMessage(new TextComponent(Configuration.insuficientargs));
            return;
        }
        if (p.getName().equalsIgnoreCase(args[0])){
            p.sendMessage(new TextComponent(Configuration.reporturself));
            return;
        }
        if (disabled_servers.contains(((ProxiedPlayer) s).getServer().getInfo().getName())){
            s.sendMessage(new TextComponent(Configuration.disabled_server));
            return;
        }
        if (cooldown.containsKey(((ProxiedPlayer) s).getUniqueId())){
            if (System.currentTimeMillis() - rep_cooldown < cooldown.get(((ProxiedPlayer) s).getUniqueId())){
                s.sendMessage(new TextComponent(Configuration.report_cooldown));
                return;
            } else {
                cooldown.remove(((ProxiedPlayer) s).getUniqueId());
            }
        }
        if (ProxyServer.getInstance().getPlayer(args[0]) == null) {
            p.sendMessage(new TextComponent(Configuration.offlinereported.replace("{player}", args[0])));
            return;
        }
        if (ProxyServer.getInstance().getPlayer(args[0]).hasPermission("reporting.antireport") ||
                ProxyServer.getInstance().getPlayer(args[0]).hasPermission("reporting.*")){
            p.sendMessage(new TextComponent(Configuration.anti_report));
            return;
        }
        StringBuilder reason = new StringBuilder();
        for (int i = 1; i < args.length; i++) {
            reason.append(args[i] + " ").toString();
        }
        p.sendMessage(new TextComponent(Configuration.reportsent));
        String r_server = plugin.getProxy().getPlayer(args[0]).getServer().getInfo().getName();
        TextComponent ms = new TextComponent(Configuration.chatmsg.replace("{reported}", args[0]).replace("{reporter}", p.getName()).replace("{reason}", reason.toString())
                .replace("{server}",r_server));
        for (ProxiedPlayer admin : ProxyServer.getInstance().getPlayers()) {
            if (!(admin.hasPermission("reporting.receive") || admin.hasPermission("reporting.*"))) {
                continue;
            }
            if (teleport) {
                ms.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/gotosv " + r_server));
            }
            admin.sendMessage(ms);
            if (plugin.titles) {
                Titles.sendTitle(Configuration.titletitle, Configuration.titlesubtitle.replace("{reported}", args[0]).replace("{server}", r_server), admin);
            }
            if (plugin.action) {
                Titles.sendAction(Configuration.actionmsg.replace("{reported}", args[0]).replace("{reason}", reason).replace("{server}", r_server), admin);
            }
        }
        cooldown.put(((ProxiedPlayer) s).getUniqueId(), System.currentTimeMillis());
        if (plugin.mysql){
            int i = 0;
            if (ProxyServer.getInstance().getPlayer(args[0]) != null){
                MySQL m = new MySQL(Database.host, Database.port, Database.database, Database.username, Database.password);
                if (m.isDataExists(Database.playertable, "Player", ProxyServer.getInstance().getPlayer(args[0]).getUniqueId().toString())){
                    i = (int) new MySQL(Database.host, Database.port, Database.database, Database.username,
                            Database.password).getReportedTimes(Database.playertable, ProxyServer.getInstance().getPlayer(args[0]).getUniqueId());
                } else {
                    i = (int) new MySQL(Database.host, Database.port, Database.database, Database.username,
                            Database.password).getReportedTimes(Database.playertable, args[0]);
                }
            }
            if (i >= plugin.suspect) {
                for (ProxiedPlayer admin : ProxyServer.getInstance().getPlayers()) {
                    if (!(admin.hasPermission("reporting.receive") || admin.hasPermission("reporting.*"))) {
                        continue;
                    }
                    admin.sendMessage(new TextComponent(Configuration.chatmsgalert.replace("{times}", String.valueOf(i))));
                }
            }
            Database.saveReport(p, ProxyServer.getInstance().getPlayer(args[0]), reason.toString());
        } else {
            int i = 0;
            if (ProxyServer.getInstance().getPlayer(args[0]) != null) {
                SQLite m = new SQLite();
                if (m.isDataExists(Database.playertable, "Player", ProxyServer.getInstance().getPlayer(args[0]).getUniqueId().toString())) {
                    i = (int) m.getReportedTimes(Database.playertable, ProxyServer.getInstance().getPlayer(args[0]).getUniqueId());
                } else {
                    i = (int) m.getReportedTimes(Database.playertable, args[0]);
                }
            }
            if (i >= plugin.suspect) {
                for (ProxiedPlayer admin : ProxyServer.getInstance().getPlayers()) {
                    if (!(admin.hasPermission("reporting.receive") || admin.hasPermission("reporting.*"))) {
                        continue;
                    }
                    admin.sendMessage(new TextComponent(Configuration.chatmsgalert.replace("{times}", String.valueOf(i))));
                }

            }
            Database.saveReport(p, ProxyServer.getInstance().getPlayer(args[0]), reason.toString());
        }
    }

    @Override
    public Iterable<String> onTabComplete(CommandSender s, String[] args) {
        if (args.length == 1){
            if (args[0] != null) {
                ArrayList<String> results = new ArrayList<>();
                String search = args[0].toLowerCase();
                for (ProxiedPlayer p : plugin.getProxy().getPlayers()) {
                    if (p.getName().toLowerCase().startsWith(search)) {
                        results.add(p.getName());
                    }
                }
                return results;
            }
            return new ArrayList<>();
        }
        return new ArrayList<>();
    }
}
