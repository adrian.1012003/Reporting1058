package com.andrei1058.reporting.bungee.commands;

import com.andrei1058.reporting.bungee.misc.MySQL;
import com.andrei1058.reporting.bungee.misc.SQLite;
import com.andrei1058.reporting.bungee.settings.Configuration;
import com.andrei1058.reporting.bungee.settings.Database;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import static com.andrei1058.reporting.bungee.Main.*;
import static com.andrei1058.reporting.bungee.commands.ReportsList.isInt;

public class CloseReport extends Command {
    public CloseReport(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender s, String[] args) {
        if (!(plugin.mysql || sqlite)) {
            s.sendMessage(new TextComponent(Configuration.not_available));
            return;
        }
        if (args.length < 2) {
            s.sendMessage(new TextComponent(Configuration.close_rep_usage));
            return;
        }
        if (!isInt(args[0])) {
            s.sendMessage(new TextComponent(Configuration.close_rep_usage));
            return;
        }
        if (!(s.hasPermission("reporting.close") || s.hasPermission("reporting.*"))) {
            s.sendMessage(new TextComponent(Configuration.permission));
            return;
        }
        StringBuilder verdict = new StringBuilder();
        for (int i = 1; i < args.length; i++) {
            verdict.append(args[i] + " ").toString();
        }
        if (mysql) {
            MySQL m = new MySQL(Database.host, Database.port, Database.database, Database.username, Database.password);
            if (!m.isDataExists(Database.maintable, "ID", args[0])) {
                s.sendMessage(new TextComponent(Configuration.rep_not_found));
                m.close();
                return;
            }
            if (m.isClosed(args[0])) {
                s.sendMessage(new TextComponent(Configuration.already_closed));
                m.close();
                return;
            }
            m.closeReport(args[0], String.valueOf(verdict).replace("'", "''"), (ProxiedPlayer) s);
        } else {
            SQLite m = new SQLite();
            if (!m.isDataExists(Database.maintable, "ID", args[0])) {
                s.sendMessage(new TextComponent(Configuration.rep_not_found));
                return;
            }
            if (m.isClosed(args[0])) {
                s.sendMessage(new TextComponent(Configuration.already_closed));
                return;
            }
            m.closeReport(args[0], String.valueOf(verdict).replace("'", "''"), (ProxiedPlayer) s);
        }
        s.sendMessage(new TextComponent(Configuration.rep_closed.replace("{id}",args[0])));
    }
}
