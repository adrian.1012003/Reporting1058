package com.andrei1058.reporting.bungee.commands;

import com.andrei1058.reporting.bungee.misc.MySQL;
import com.andrei1058.reporting.bungee.misc.SQLite;
import com.andrei1058.reporting.bungee.settings.Configuration;
import com.andrei1058.reporting.bungee.settings.Database;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.plugin.Command;

import static com.andrei1058.reporting.bungee.Main.*;

public class DelReport extends Command {

    public DelReport(String name){
        super(name);
    }

    @Override
    public void execute(CommandSender s, String[] args) {
        if (!(plugin.mysql || sqlite)) {
            s.sendMessage(new TextComponent(Configuration.not_available));
            return;
        }
        if (args.length == 0 || args.length > 1){
            s.sendMessage(new TextComponent(Configuration.delrep_usage));
            return;
        }
        if (!ReportsList.isInt(args[0])){
            s.sendMessage(new TextComponent(Configuration.delrep_usage));
            return;
        }
        if (!(s.hasPermission("reporting.delete") || s.hasPermission("reporting.*"))){
            s.sendMessage(new TextComponent(Configuration.permission));
            return;
        }
        if (mysql) {
            MySQL m = new MySQL(Database.host, Database.port, Database.database, Database.username, Database.password);
            if (!m.isDataExists(Database.maintable, "ID", args[0])) {
                s.sendMessage(new TextComponent(Configuration.rep_not_found));
                m.close();
                return;
            }
            m.deleteData(Database.maintable, "ID", "=", args[0]);

            m.close();
        } else {
            SQLite m = new SQLite();
            if (!m.isDataExists(Database.maintable, "ID", args[0])) {
                s.sendMessage(new TextComponent(Configuration.rep_not_found));
                return;
            }
            m.deleteData(Database.maintable, "ID", "=", args[0]);
        }
        s.sendMessage(new TextComponent(Configuration.rep_deleted));
    }
}
