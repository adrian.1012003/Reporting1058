package com.andrei1058.reporting.bungee.commands;

import com.andrei1058.reporting.bungee.misc.MySQL;
import com.andrei1058.reporting.bungee.misc.SQLite;
import com.andrei1058.reporting.bungee.settings.Configuration;
import com.andrei1058.reporting.bungee.settings.Database;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.plugin.Command;

import java.util.ArrayList;
import java.util.UUID;

import static com.andrei1058.reporting.bungee.Main.*;
import static com.andrei1058.reporting.bungee.commands.ReportsList.isInt;

public class MostReported extends Command{

    public MostReported(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender s, String[] args) {
        if (!(s.hasPermission("reporting.list")|| s.hasPermission("reporting.*"))){
            s.sendMessage(new TextComponent(Configuration.permission));
            return;
        }
        if (!(mysql || sqlite)){
            s.sendMessage(new TextComponent(Configuration.not_available));
            return;
        }
        if (args.length <= 1) {
            int start = 0;
            int limit = r_p_p;
            if (args.length == 1) {
                if (isInt(args[0])) {
                    limit = r_p_p * Integer.parseInt(args[0]) + 1;
                    start = r_p_p * (Integer.parseInt(args[0]) - 1);
                } else {
                    s.sendMessage(new TextComponent(Configuration.mostreported_usage));
                }
            }
            s.sendMessage(new TextComponent(Configuration.loading));
            ArrayList list = new ArrayList();
            list.add("Player");
            list.add("PlayerName");
            list.add("Times");

            ArrayList<ArrayList> lista;
            if (mysql) {
                MySQL m = new MySQL(Database.host, Database.port, Database.database, Database.username, Database.password);
                lista = m.getTopReports(Database.playertable, list, String.valueOf(start), String.valueOf(limit));
            } else {
                lista = new SQLite().getTopReports(Database.playertable, list, String.valueOf(start), String.valueOf(limit));
            }
            if (lista.size() == 0){
                s.sendMessage(new TextComponent(Configuration.no_active_reports));
                return;
            }
            for (Object st : lista) {
                ArrayList a = (ArrayList) st;
                String player;
                if (plugin.getProxy().getPlayer(UUID.fromString(String.valueOf(a.get(0)))) != null) {
                    player = plugin.getProxy().getPlayer(UUID.fromString(String.valueOf(a.get(0)))).getName();
                } else {
                    player = (String) a.get(1);
                }
                String reported = (String) a.get(2);
                start++;
                s.sendMessage(new TextComponent(Configuration.mostreported_list.replace("{username}", player).replace("{nr}", String.valueOf(start)).replace("{reports}", reported)));
            }
        }
    }
}
