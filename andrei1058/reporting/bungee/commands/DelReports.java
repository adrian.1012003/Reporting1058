package com.andrei1058.reporting.bungee.commands;

import com.andrei1058.reporting.bungee.misc.MySQL;
import com.andrei1058.reporting.bungee.misc.SQLite;
import com.andrei1058.reporting.bungee.settings.Configuration;
import com.andrei1058.reporting.bungee.settings.Database;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.plugin.Command;

import static com.andrei1058.reporting.bungee.Main.*;

public class DelReports extends Command {
    public DelReports(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender s, String[] args) {
        if (!(plugin.mysql || sqlite)) {
            s.sendMessage(new TextComponent(Configuration.not_available));
            return;
        }
        if (args.length == 0 || args.length > 1){
            s.sendMessage(new TextComponent(Configuration.del_reports_usage_player));
            return;
        }
        if (ReportsList.isInt(args[0])){
            s.sendMessage(new TextComponent(Configuration.del_reports_usage_player));
            return;
        }
        if (!(s.hasPermission("reporting.deleteallplayer")|| s.hasPermission("reporting.*"))){
            s.sendMessage(new TextComponent(Configuration.permission));
            return;
        }
        if (mysql) {
            MySQL m = new MySQL(Database.host, Database.port, Database.database, Database.username, Database.password);
            if (plugin.getProxy().getPlayer(args[0]) != null && plugin.getProxy().getPlayer(args[0]).getUniqueId() != null && m.isDataExists(Database.maintable, "Reported", plugin.getProxy().getPlayer(args[0]).getUniqueId().toString())) {
                m.deleteData(Database.maintable, "Reported", "=", plugin.getProxy().getPlayer(args[0]).getUniqueId().toString());
                m.deleteData(Database.playertable, "Player", "=", plugin.getProxy().getPlayer(args[0]).getUniqueId().toString());
                s.sendMessage(new TextComponent(Configuration.reports_deleted_player.replace("{player}", args[0])));
            } else if (m.isDataExists(Database.maintable, "ReportedName", args[0])) {
                m.deleteData(Database.maintable, "ReportedName", "=", args[0]);
                m.deleteData(Database.playertable, "PlayerName", "=", args[0]);
                s.sendMessage(new TextComponent(Configuration.reports_deleted_player.replace("{player}", args[0])));
            } else {
                s.sendMessage(new TextComponent(Configuration.no_data_found_player_del_reports));
            }
            m.close();
        } else {
            SQLite m = new SQLite();
            if (plugin.getProxy().getPlayer(args[0]) != null && plugin.getProxy().getPlayer(args[0]).getUniqueId() != null && m.isDataExists(Database.maintable, "Reported", plugin.getProxy().getPlayer(args[0]).getUniqueId().toString())) {
                m.deleteData(Database.maintable, "Reported", "=", plugin.getProxy().getPlayer(args[0]).getUniqueId().toString());
                m.deleteData(Database.playertable, "Player", "=", plugin.getProxy().getPlayer(args[0]).getUniqueId().toString());
                s.sendMessage(new TextComponent(Configuration.reports_deleted_player.replace("{player}", args[0])));
            } else if (m.isDataExists(Database.maintable, "ReportedName", args[0])) {
                m.deleteData(Database.maintable, "ReportedName", "=", args[0]);
                m.deleteData(Database.playertable, "PlayerName", "=", args[0]);
                s.sendMessage(new TextComponent(Configuration.reports_deleted_player.replace("{player}", args[0])));
            } else {
                s.sendMessage(new TextComponent(Configuration.no_data_found_player_del_reports));
            }
        }
    }
}
