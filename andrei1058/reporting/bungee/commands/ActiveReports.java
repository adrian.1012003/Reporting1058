package com.andrei1058.reporting.bungee.commands;

import com.andrei1058.reporting.bungee.misc.MySQL;
import com.andrei1058.reporting.bungee.misc.SQLite;
import com.andrei1058.reporting.bungee.settings.Configuration;
import com.andrei1058.reporting.bungee.settings.Database;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.plugin.Command;

import java.util.ArrayList;
import java.util.UUID;

import static com.andrei1058.reporting.bungee.Main.*;
import static com.andrei1058.reporting.bungee.commands.ReportsList.isInt;

public class ActiveReports extends Command {
    public ActiveReports(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender s, String[] args) {
        if (!(mysql || sqlite)) {
            s.sendMessage(new TextComponent(Configuration.not_available));
            return;
        }
        if (!(s.hasPermission("reporting.list") || s.hasPermission("reporting.*"))){
            s.sendMessage(new TextComponent(Configuration.permission));
            return;
        }
        if (args.length <= 1) {
            int start = 0;
            int limit = r_p_p;
            if (args.length == 1) {
                if (isInt(args[0])) {
                    limit = r_p_p * Integer.parseInt(args[0])+1;
                    start = r_p_p * (Integer.parseInt(args[0])-1);
                } else {
                    s.sendMessage(new TextComponent(Configuration.active_reports_usage));
                }
            }
            s.sendMessage(new TextComponent(Configuration.loading));
            ArrayList list = new ArrayList();
            list.add("ID");
            list.add("Reporter");
            list.add("Reported");
            list.add("Reason");
            list.add("Server");
            list.add("Date");
            list.add("ReporterName");
            list.add("ReportedName");
            list.add("Status");
            ArrayList<ArrayList> lista = new ArrayList<>();
            if (mysql) {
                MySQL m = new MySQL(Database.host, Database.port, Database.database, Database.username, Database.password);
                lista = m.getActiveReports(Database.maintable, list, String.valueOf(start), String.valueOf(limit));
            } else {
                lista = new SQLite().getActiveReports(Database.maintable, list, String.valueOf(start), String.valueOf(limit));
            }
            if (lista.size() == 0){
                s.sendMessage(new TextComponent(Configuration.no_active_reports));
                return;
            }
            for (Object st : lista) {
                ArrayList a = (ArrayList) st;
                String id = (String) a.get(0);
                String reporter;
                String reported;
                String reason;
                String server = (String) a.get(4);
                String date = (String) a.get(5);
                String status;
                if (a.get(8).equals("1")){
                    status = Configuration.status_active;
                } else {
                    status = Configuration.status_closed;
                }
                if (ProxyServer.getInstance().getPlayer(UUID.fromString((String) a.get(1))) != null) {
                    reporter = ProxyServer.getInstance().getPlayer(UUID.fromString((String) a.get(1))).getName();
                } else {
                    reporter = (String) a.get(6);
                }
                if (ProxyServer.getInstance().getPlayer(UUID.fromString((String) a.get(2))) != null) {
                    reported = ProxyServer.getInstance().getPlayer(UUID.fromString((String) a.get(2))).getName();
                } else {
                    reported = (String) a.get(7);
                }
                reason = (String) a.get(3);
                s.sendMessage(new TextComponent(Configuration.report_list_active.replace("{id}", id).replace("{server}", server).replace("{reported}", reported)
                        .replace("{reason}", reason).replace("{reporter}", reporter).replace("{date}", date).replace("{status}", status)));
            }
            if (args.length == 0){
                s.sendMessage(new TextComponent(Configuration.areports_next_page.replace("{page}", "2")));
            } else {
                s.sendMessage(new TextComponent(Configuration.areports_next_page.replace("{page}", String.valueOf(Integer.parseInt(args[0])+1))));
            }
        } else {
            s.sendMessage(new TextComponent(Configuration.active_reports_usage));
        }

    }
}
