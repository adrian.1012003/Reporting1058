package com.andrei1058.reporting.bungee.commands;

import com.andrei1058.reporting.bungee.settings.Configuration;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import static com.andrei1058.reporting.bungee.settings.Configuration.setupConfig;

/**
 * Copyright Andrei Dascalu - andrei1058 @spigotmc.org
 * Reporting1058 class written on 07/04/2017
 */
public class ReportReload extends Command {
    public ReportReload(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender s, String[] args) {
        if (s instanceof ProxiedPlayer) {
            ProxiedPlayer p = (ProxiedPlayer) s;
            if (!(p.hasPermission("reporting.reload") || p.hasPermission("reporting.*"))) {
                p.sendMessage(new TextComponent(Configuration.permission));
                return;
            }
        }
        setupConfig();
        s.sendMessage(new TextComponent("§9Reporting1058 §8» §aConfig reloaded!"));
    }
}
