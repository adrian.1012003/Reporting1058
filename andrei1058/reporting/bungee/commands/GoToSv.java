package com.andrei1058.reporting.bungee.commands;

import com.andrei1058.reporting.bungee.settings.Configuration;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 * Copyright Andrei Dascalu - andrei1058 @spigotmc.org
 * Reporting1058 class written on 22/02/2017
 */
public class GoToSv extends Command{

    public GoToSv(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender s, String[] args) {
        if (!(s.hasPermission("reporting.teleport") || s.hasPermission("reporting.*"))){
            s.sendMessage(new TextComponent(Configuration.permission));
            return;
        }
        if (args.length == 1){
            ProxiedPlayer p = (ProxiedPlayer) s;
            ServerInfo sv = ProxyServer.getInstance().getServerInfo(args[0]);
            p.connect(sv);
        } else {
            s.sendMessage(new TextComponent(Configuration.permission));
            return;
        }
    }
}
