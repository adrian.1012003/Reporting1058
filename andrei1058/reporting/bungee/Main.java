package com.andrei1058.reporting.bungee;

import com.andrei1058.reporting.bungee.commands.*;
import com.andrei1058.reporting.bungee.metrics.bStats;
import com.andrei1058.reporting.bungee.misc.Listeners;
import com.andrei1058.reporting.bungee.misc.Updater;
import com.andrei1058.reporting.bungee.settings.Configuration;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.api.plugin.PluginManager;

import java.util.ArrayList;

public class Main extends Plugin {
    public static Main plugin;
    public static boolean titles = true;
    public static boolean action = true;
    public static boolean mysql = false;
    public static int suspect = 5;
    public static long rep_cooldown = 20000;
    public static boolean motd = true;
    public static int r_p_p = 7;
    public static ArrayList<String> disabled_servers = new ArrayList<>();
    public static boolean update = false;
    public static String newVersion;
    public static boolean teleport;
    public static boolean sqlite = true;
    @Override
    public void onEnable() {
        plugin = this;
        Configuration.setupConfig();
        PluginManager pm = getProxy().getPluginManager();
        pm.registerCommand(this, new Report());
        pm.registerCommand(this, new ReportsList("reportslist"));
        pm.registerCommand(this, new ReportsList("reportlist"));
        pm.registerCommand(this, new ReportsList("reports"));
        pm.registerCommand(this, new ReportsList("rlist"));
        pm.registerCommand(this, new DelReport("delreport"));
        pm.registerCommand(this, new DelReport("rdel"));
        pm.registerCommand(this, new Reporting1058("reporting1058"));
        pm.registerCommand(this, new Reporting1058("com/andrei1058/reporting"));
        pm.registerCommand(this, new Reporting1058("rhelp"));
        pm.registerCommand(this, new ReportInfo("reportinfo"));
        pm.registerCommand(this, new ReportInfo("rinfo"));
        pm.registerCommand(this, new CloseReport("closereport"));
        pm.registerCommand(this, new CloseReport("rclose"));
        pm.registerCommand(this, new ActiveReports("areports"));
        pm.registerCommand(this, new ActiveReports("activereports"));
        pm.registerCommand(this, new CloseReports());
        pm.registerListener(this, new Listeners());
        pm.registerCommand(this, new DelReports("delreports"));
        pm.registerCommand(this, new DelReports("rdelall"));
        pm.registerCommand(this, new MostReported("mostreported"));
        pm.registerCommand(this, new MostReported("mreported"));
        pm.registerCommand(this, new MostReported("rmost"));
        pm.registerCommand(this, new GoToSv("gotosv"));
        pm.registerCommand(this, new ReportReload("rreload"));
        new bStats(this);
        Updater.checkUpdates();
    }
}
