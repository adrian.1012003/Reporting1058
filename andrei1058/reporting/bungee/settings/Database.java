package com.andrei1058.reporting.bungee.settings;

import com.andrei1058.reporting.bungee.misc.MySQL;
import com.andrei1058.reporting.bungee.misc.SQLite;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static com.andrei1058.reporting.bungee.Main.*;

public class Database {
    public static String host = null;
    public static int port = 0;
    public static String database = null;
    public static String username = null;
    public static String password = null;
    public static String maintable = null;
    public static String playertable = null;
    public static SimpleDateFormat date = new SimpleDateFormat(Configuration.dateformat);

    public static void setup() {
        if (plugin.mysql) {
            ArrayList<String> records = new ArrayList<String>();
            records.add("ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY");
            records.add("Reporter char(36)");
            records.add("Reported char(36)");
            records.add("Reason char(50)");
            records.add("Server char(36)");
            records.add("Date char(36)");
            records.add("ReporterName char(36)");
            records.add("ReportedName char(36)");
            records.add("Status char(36)");
            records.add("Verdict char(36)");
            records.add("Moderator char(36)");
            records.add("ModeratorName char(36)");
            records.add("ClosedOn char(36)");
            MySQL m = new MySQL(host, port, database, username, password);
            m.createTable(maintable, records);

            ArrayList<String> records2 = new ArrayList<>();
            records2.add("Player char(36)");
            records2.add("PlayerName char(36)");
            records2.add("Times INT");
            MySQL m2 = new MySQL(host, port, database, username, password);
            m2.createTable(playertable, records2);
        } else {
            ArrayList<String> records = new ArrayList<String>();
            records.add("ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL");
            records.add("Reporter (36)");
            records.add("Reported char(36)");
            records.add("Reason char(50)");
            records.add("Server char(36)");
            records.add("Date char(36)");
            records.add("ReporterName char(36)");
            records.add("ReportedName char(36)");
            records.add("Status char(36)");
            records.add("Verdict char(36)");
            records.add("Moderator char(36)");
            records.add("ModeratorName char(36)");
            records.add("ClosedOn char(36)");
            new SQLite().createTable(maintable, records);

            ArrayList<String> records2 = new ArrayList<>();
            records2.add("Player char(36)");
            records2.add("PlayerName char(36)");
            records2.add("Times INT");
            new SQLite().createTable(playertable, records2);
        }
    }

    public static void saveReport(ProxiedPlayer Reporter, ProxiedPlayer Reported, String Reason){
        if (mysql){
            MySQL m = new MySQL(host, port, database, username, password);
            m.connect();
            m.addReportAsNumber(playertable, Reported.getName(), Reported);
            ArrayList main1 = new ArrayList();
            ArrayList main2 = new ArrayList();
            main1.add("Reporter");
            main1.add("Reported");
            main1.add("Reason");
            main1.add("Server");
            main1.add("Date");
            main1.add("ReporterName");
            main1.add("ReportedName");
            main1.add("Status");
            main2.add(Reporter.getUniqueId().toString());
            main2.add(Reported.getUniqueId().toString());
            main2.add(new String(Reason.replace("'", "''")));
            main2.add(Reported.getServer().getInfo().getName());
            main2.add(date.format(new Date()));
            main2.add(Reporter.getName());
            main2.add(Reported.getName());
            main2.add("1");
            m.createDate(maintable, main1, main2);
            m.close();
        } else if (sqlite){
            new SQLite().addReportAsNumber(playertable, Reported.getName(), Reported);
            ArrayList main1 = new ArrayList();
            ArrayList main2 = new ArrayList();
            main1.add("Reporter");
            main1.add("Reported");
            main1.add("Reason");
            main1.add("Server");
            main1.add("Date");
            main1.add("ReporterName");
            main1.add("ReportedName");
            main1.add("Status");
            main2.add(Reporter.getUniqueId().toString());
            main2.add(Reported.getUniqueId().toString());
            main2.add(new String(Reason.replace("'", "''")));
            main2.add(Reported.getServer().getInfo().getName());
            main2.add(date.format(new Date()));
            main2.add(Reporter.getName());
            main2.add(Reported.getName());
            main2.add("1");
            new SQLite().createDate(maintable, main1, main2);
        }
    }
}
