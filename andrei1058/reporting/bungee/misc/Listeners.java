package com.andrei1058.reporting.bungee.misc;

import com.andrei1058.reporting.bungee.settings.Configuration;
import com.andrei1058.reporting.bungee.settings.Database;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.event.ServerSwitchEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import static com.andrei1058.reporting.bungee.Main.*;

public class Listeners implements Listener {

    @EventHandler
    public void s(ServerSwitchEvent e) {
        if (mysql && motd) {
            if (e.getPlayer().hasPermission("reporting.motd")) {
                if (disabled_servers.contains(e.getPlayer().getServer().getInfo().getName())) return;
                MySQL m = new MySQL(Database.host, Database.port, Database.database, Database.username, Database.password);
                int x = m.getActiveByServer(e.getPlayer().getServer().getInfo().getName());
                if (x != 0) {
                    e.getPlayer().sendMessage(new TextComponent(Configuration.current_server_active_reports.replace("{reports}", String.valueOf(x))));
                }
            }
        }
        if (update){
            if (e.getPlayer().hasPermission("reporting.*") || e.getPlayer().hasPermission("reporting.delete")){
                e.getPlayer().sendMessage(new TextComponent("§9Reporting1058 §8» §aThere is a new version available!"));
                e.getPlayer().sendMessage(new TextComponent("§9[§7"+newVersion+"§9] §ewww.spigotmc.org/resources/19751/"));
            }
        }
    }
}