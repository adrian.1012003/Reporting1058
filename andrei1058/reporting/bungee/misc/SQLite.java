package com.andrei1058.reporting.bungee.misc;

import com.andrei1058.reporting.bungee.settings.Database;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import org.sqlite.JDBC;

import java.io.File;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.UUID;
import java.util.logging.Level;

import static com.andrei1058.reporting.bungee.Main.plugin;
import static com.andrei1058.reporting.bungee.Main.sqlite;
import static com.andrei1058.reporting.bungee.settings.Database.date;

/**
 * Copyright Andrei Dascalu - andrei1058 @spigotmc.org
 * Reporting1058 class written on 24/04/2017
 */
public class SQLite {
    private static String dbname;
    private static Connection connection;

    public SQLite() {
        dbname = "reports";
    }

    public boolean createTable(String table, ArrayList<String> records) {
        openConnection();
        if (isConnected()) {
            try {
                boolean first = true;
                String records2 = "";
                for (String record : records) {
                    if (records.size() == 1) {
                        records2 = records2 + record;
                    }
                    else if (first) {
                        records2 = records2 + record;
                        first = false;
                    }
                    else {
                        records2 = records2 + ", " + record;
                    }
                }
                getConnection().createStatement().executeUpdate("CREATE TABLE IF NOT EXISTS `" + table + "` (" + records2 + ");");
                return true;
            }
            catch (SQLException e) {
                printError(e.getMessage());
            }
        }
        return false;
    }
    public Object getReportedTimes(String table, UUID reported) {
        if (!isConnected())
            connect();
        if (isConnected()) {
            try {
                ResultSet rs = getConnection().createStatement().executeQuery("SELECT * FROM `" + table + "` WHERE `" + "Player" + "`='" + reported.toString() + "';");
                if (rs.next()) {
                    return rs.getObject("Times");
                }
            }
            catch (SQLException e) {
            }
        }
        return 0;
    }

    public Object getReportedTimes(String table, String reported) {
        if (!isConnected())
            connect();
        if (isConnected()) {
            try {
                ResultSet rs = getConnection().createStatement().executeQuery("SELECT * FROM `" + table + "` WHERE `" + "PlayerName" + "`='" + reported + "';");
                if (rs.next()) {
                    return rs.getObject("Times");
                }
            }
            catch (SQLException e) {
            }
        }
        return 0;
    }

    public boolean deleteData(String table, String wherecolumn, String whereoperator, String wherevalue) {
        openConnection();
        if (isConnected()) {
            try {
                getConnection().createStatement().executeUpdate("DELETE FROM `" + table + "` WHERE `" + wherecolumn + "`" + whereoperator + "'" + wherevalue + "';");

                return true;
            }
            catch (SQLException e)
            {
                printError(e.getMessage());
            }
        }
        return false;
    }

    public boolean isDataExists(String table, String recordname, String record) {
        openConnection();
        if (isConnected()) {
            try {
                ResultSet rs = getConnection().createStatement().executeQuery("SELECT * FROM `" + table + "` WHERE `" + recordname + "`='" + record + "';");
                if (rs.next()) {
                    return true;
                }
            }
            catch (SQLException e)
            {
                printError(e.getMessage());
            }
        }
        return false;
    }

    public ArrayList<String> getReport(String recordname, String record) {
        openConnection();
        ArrayList rezultat = new ArrayList<>();
        if (isConnected()) {
            try {
                ResultSet rs = getConnection().createStatement().executeQuery("SELECT * FROM `" + Database.maintable + "` WHERE `" + recordname + "`='" + record + "';");
                if (rs.next()) {
                    rezultat.add(rs.getObject("ID"));
                    rezultat.add(rs.getObject("Reporter"));
                    rezultat.add(rs.getObject("Reported"));
                    rezultat.add(rs.getObject("Reason"));
                    rezultat.add(rs.getObject("Server"));
                    rezultat.add(rs.getObject("Date"));
                    rezultat.add(rs.getObject("ReporterName"));
                    rezultat.add(rs.getObject("ReportedName"));
                    rezultat.add(rs.getObject("Status"));
                    rezultat.add(rs.getObject("Verdict"));
                    rezultat.add(rs.getObject("Moderator"));
                    rezultat.add(rs.getObject("ModeratorName"));
                    rezultat.add(rs.getObject("ClosedOn"));
                    return rezultat;
                }
            } catch (SQLException e) {
                printError(e.getMessage());
            }
        }
        return null;
    }
    public void closeReport(String id, String verdict, ProxiedPlayer moderator){
        if (!isConnected()){
            openConnection();
        }
        try {
            getConnection().createStatement().executeUpdate("UPDATE `"+Database.maintable+"` SET Status=0, Verdict='"+verdict+"', ClosedOn='"+String.valueOf(date.format(new java.util.Date()))+"', Moderator='"+moderator.getUniqueId().toString()+"', ModeratorName='"+moderator.getName()+"' WHERE ID="+id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void closeReports(ProxiedPlayer moderator){
        if (!isConnected()){
            openConnection();
        }
        try {
            getConnection().createStatement().executeUpdate("UPDATE `"+Database.maintable+"` SET Status=0, ClosedOn='"+date.format(new java.util.Date()).toString()+"', Moderator='"+moderator.getUniqueId().toString()+"', ModeratorName='"+moderator.getName()+"', Verdict='"+String.valueOf("-")+"' WHERE Status=1;");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public boolean isClosed(String id){
        if (!isConnected()){
            openConnection();
        }
        try {
            ResultSet rs = getConnection().createStatement().executeQuery("SELECT Status FROM `"+Database.maintable+"` WHERE ID="+id);
            if (rs.next()){
                if (rs.getInt("Status") == 0){
                    return true;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
    public ArrayList<ArrayList<String>> getData(String table, ArrayList<String> records, String start, String limit) {
        if (!isConnected()){
            openConnection();
        }
        if (isConnected()) {
            try {
                int index = 0;
                ArrayList<ArrayList<String>> records2 = new ArrayList();
                ResultSet rs = getConnection().createStatement().executeQuery("SELECT * FROM `" + table + "` ORDER BY ID DESC LIMIT " + start + ", " + limit + ";");
                while (rs.next()) {
                    ArrayList<String> record2 = new ArrayList();
                    for (String record : records) {
                        record2.add(rs.getString(record));
                    }
                    records2.add(index, record2);
                    index++;
                }
                return records2;
            } catch (SQLException e) {
                return new ArrayList<>();
            }
        }
        return new ArrayList<>();
    }
    public int getActiveByServer(String server) {
        openConnection();
        if (isConnected()) {
            try {
                int index = 0;
                ResultSet rs = getConnection().createStatement().executeQuery("SELECT * FROM `"+Database.maintable+"` WHERE Server='"+server+"' AND Status='1';");
                while (rs.next()) {
                    index++;
                }
                return index;
            } catch (SQLException e) {
                return 0;
            }
        }
        return 0;
    }

    public ArrayList<ArrayList<String>> getActiveReports(String table, ArrayList<String> records, String start, String limit)
    {
        openConnection();
        if (isConnected()) {
            try
            {
                int index = 0;
                ArrayList<ArrayList<String>> records2 = new ArrayList();
                ResultSet rs = getConnection().createStatement().executeQuery("SELECT * FROM `" + table + "` WHERE Status='1' ORDER BY ID DESC LIMIT "+start+", "+limit+";");
                while (rs.next())
                {
                    ArrayList<String> record2 = new ArrayList();
                    for (String record : records) {
                        record2.add(rs.getString(record));
                    }
                    records2.add(index, record2);
                    index++;
                }
                return records2;
            }
            catch (SQLException e)
            {
                printError(e.getMessage());
            }
        }

        return new ArrayList();
    }

    public ArrayList<ArrayList<String>> getTopReports(String table, ArrayList<String> records, String start, String limit) {
        openConnection();
        if (isConnected()) {
            try {
                int index = 0;
                ArrayList<ArrayList<String>> records2 = new ArrayList();
                ResultSet rs = getConnection().createStatement().executeQuery("SELECT * FROM `" + table + "` ORDER BY Times DESC LIMIT " + start + ", " + limit + ";");
                while (rs.next()) {
                    ArrayList<String> record2 = new ArrayList();
                    for (String record : records) {
                        record2.add(rs.getString(record));
                    }
                    records2.add(index, record2);
                    index++;
                }
                return records2;
            } catch (SQLException e) {
                printError(e.getMessage());
            }
        }

        return new ArrayList();
    }
    public void addReportAsNumber(String table, String playerName, ProxiedPlayer player) {
        if (!isConnected()) {
            openConnection();
        }
        if (isConnected()) {
            if (player != null) {
                try {
                    ResultSet rs = getConnection().createStatement().executeQuery("SELECT Times FROM `" + table + "` WHERE Player='" + player.getUniqueId().toString() + "'");
                    if (rs.next()) {
                        int times = rs.getInt("Times");
                        getConnection().createStatement().executeUpdate("UPDATE `" + table + "` SET Times=" + String.valueOf(times + 1) + " WHERE Player='" + player.getUniqueId() + "'");
                    } else {
                        getConnection().createStatement().executeUpdate("INSERT INTO `" + table + "` (Player,PlayerName,Times) VALUES ('" + player.getUniqueId().toString() + "','" + playerName + "','1');");
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    ResultSet rs = getConnection().createStatement().executeQuery("SELECT Times FROM `" + table + "` WHERE PlayerName='" + playerName + "'");
                    if (rs.next()) {
                        int times = rs.getInt("Times");
                        getConnection().createStatement().executeUpdate("UPDATE `" + table + "` SET Times=" + String.valueOf(times + 1) + " WHERE PlayerName='" + playerName + "'");
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public ArrayList<ArrayList<String>> getPlayerReports(String table, ArrayList<String> records, String start, String limit, String name, ProxiedPlayer uuid) {
        openConnection();
        if (isConnected()) {
            if (uuid != null) {
                try {
                    int index = 0;
                    ArrayList<ArrayList<String>> records2 = new ArrayList();
                    ResultSet rs = getConnection().createStatement().executeQuery("SELECT * FROM `" + table + "` WHERE Reported='" + uuid + "' ORDER BY Status DESC LIMIT " + start + ", " + limit + ";");
                    if (rs.next()) {
                        ArrayList<String> record2 = new ArrayList();
                        for (String record : records) {
                            record2.add(rs.getString(record));
                        }
                        records2.add(index, record2);
                        index++;
                    } else {
                        try {
                            int index2 = 0;
                            ArrayList<ArrayList<String>> records23 = new ArrayList();
                            ResultSet rsx = getConnection().createStatement().executeQuery("SELECT * FROM `" + table + "` WHERE ReportedName='" + name + "' ORDER BY Status DESC LIMIT " + start + ", " + limit + ";");
                            while (rsx.next()) {
                                ArrayList<String> record23 = new ArrayList();
                                for (String record : records) {
                                    record23.add(rsx.getString(record));
                                }
                                records23.add(index2, record23);
                                index2++;
                            }
                            return records23;
                        } catch (SQLException e) {
                            printError(e.getMessage());
                        }
                    }
                    return records2;
                } catch (SQLException e) {
                    printError(e.getMessage());
                }
            } else {
                try {
                    int index2 = 0;
                    ArrayList<ArrayList<String>> records23 = new ArrayList();
                    ResultSet rsx = getConnection().createStatement().executeQuery("SELECT * FROM `" + table + "` WHERE ReportedName='"+name+"' ORDER BY Status DESC LIMIT " + start + ", " + limit + ";");
                    while (rsx.next()) {
                        ArrayList<String> record23 = new ArrayList();
                        for (String record : records) {
                            record23.add(rsx.getString(record));
                        }
                        records23.add(index2, record23);
                        index2++;
                    }
                    return records23;
                } catch (SQLException e) {
                    printError(e.getMessage());
                }
            }
        }
        return new ArrayList();
    }

    public boolean createDate(String table, ArrayList<String> recordsnames, ArrayList<String> records) {
        openConnection();
        if (isConnected()) {
            try {
                boolean first = true;
                boolean first2 = true;
                String records2 = "";
                String recordsnames2 = "";
                for (String recordname : recordsnames) {
                    if (recordsnames.size() == 1) {
                        recordsnames2 = recordsnames2 + "`" + recordname + "`";
                    }
                    else if (first) {
                        recordsnames2 = recordsnames2 + "`" + recordname + "`";
                        first = false;
                    }
                    else {
                        recordsnames2 = recordsnames2 + ", `" + recordname + "`";
                    }
                }
                for (String record : records) {
                    if (records.size() == 1) {
                        records2 = records2 + "'" + record + "'";
                    }
                    else if (first2) {
                        records2 = records2 + "'" + record + "'";
                        first2 = false;
                    }
                    else {
                        records2 = records2 + ", '" + record + "'";
                    }
                }
                getConnection().createStatement().executeUpdate("INSERT INTO `" + table + "` (" + recordsnames2 + ") VALUES (" + records2 + ");");
                return true;
            }
            catch (SQLException e)
            {
                printError(e.getMessage());
            }
        }
        return false;
    }

    public boolean execute(String inquiry) {
        openConnection();
        if (isConnected()) {
            try {
                getConnection().createStatement().executeUpdate(inquiry);

                return true;
            }
            catch (SQLException e) {
                printError(e.getMessage());
            }
        }
        
        return false;
    }

    private void printError(String error) {
        System.out.println(error);
    }




    private Connection getConnection() {
        return this.connection;
    }
    private void setConnection(Connection conn) {
        this.connection = conn;
    }
    public void connect(){
        openConnection();
    }

    private boolean openConnection() {
        if (!isConnected()) {
            File dataFolder = new File(plugin.getDataFolder(), dbname + ".db");
            if (!dataFolder.exists()) {
                try {
                    dataFolder.createNewFile();
                } catch (IOException e) {
                    plugin.getLogger().log(Level.SEVERE, "File write error: " + dbname + ".db");
                }
            }
            try {
                if (connection != null && !connection.isClosed()) {
                    return true;
                }
                try {
                    Class.forName("org.sqlite.JDBC");
                } catch (ClassNotFoundException e) {
                    DriverManager.registerDriver(JDBC.class.newInstance());
                }
                connection = DriverManager.getConnection("jdbc:sqlite:" + dataFolder);
                sqlite = true;
                return true;
            } catch (SQLException ex) {
                plugin.getLogger().log(Level.SEVERE, "SQLite exception on initialize", ex);
                sqlite = false;
                return false;
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    /*private boolean closeConnection() {
        if (isConnected()) {
            try {
                getConnection().close();
            }
            catch (SQLException e) {
                printError(e.getMessage());

                return false;
            }
        }
        return true;
    }*/

    public boolean isConnected() {
        try {
            if (getConnection() == null) {
                return false;
            }
            if (getConnection().isClosed()) {
                return false;
            }
        }
        catch (SQLException e) {
            printError(e.getMessage());
        }
        return true;
    }
}
