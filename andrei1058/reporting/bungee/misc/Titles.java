package com.andrei1058.reporting.bungee.misc;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.Title;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class Titles {
    public static void sendTitle(String titlu, String subtitle, ProxiedPlayer p) {
        Title title = ProxyServer.getInstance().createTitle();
        title.fadeIn(5);
        title.fadeOut(5);
        title.stay(20);
        title.title(new TextComponent(ChatColor.translateAlternateColorCodes('&', titlu)));
        title.subTitle(new TextComponent(ChatColor.translateAlternateColorCodes('&', subtitle)));
        title.send(p);

    }

    public static void sendAction(String s, ProxiedPlayer p) {
        p.sendMessage(ChatMessageType.ACTION_BAR, new TextComponent(ChatColor.translateAlternateColorCodes('&', s)));
    }
}
