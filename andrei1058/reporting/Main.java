package com.andrei1058.reporting;

import com.andrei1058.reporting.bukkit.commands.*;
import com.andrei1058.reporting.bukkit.metrics.bStats;
import com.andrei1058.reporting.bukkit.misc.Listeners;
import com.andrei1058.reporting.bukkit.misc.Updater;
import com.andrei1058.reporting.bukkit.settings.Config;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;

public class Main extends JavaPlugin {

    public static Main plugin;
    public static boolean titles = true;
    public static boolean action = true;
    public static boolean mysql = false;
    public static int suspect = 5;
    public static long rep_cooldown = 20000;
    public static boolean motd = true;
    public static String nmsver;
    public static boolean useOldMethods = false;
    public static int r_p_p = 7;
    public static ArrayList<String> disabled_worlds = new ArrayList<>();
    public static boolean update = false;
    public static String newVersion;
    public static boolean Bsqlite = true;
    @Override
    public void onEnable() {
        plugin = this;
        Config.setupConfig();
        nmsver = Bukkit.getServer().getClass().getPackage().getName();
        nmsver = nmsver.substring(nmsver.lastIndexOf(".") + 1);
        if ((nmsver.equalsIgnoreCase("v1_8_R1")) || (nmsver.equalsIgnoreCase("v1_7_"))) {
            useOldMethods = true;
        }
        getCommand("report").setExecutor(new Report());
        getCommand("activereports").setExecutor(new ActiveReports());
        getCommand("closereport").setExecutor(new CloseReport());
        getCommand("closereports").setExecutor(new CloseReports());
        getCommand("delreport").setExecutor(new DelReport());
        getCommand("reportinfo").setExecutor(new ReportInfo());
        getCommand("reporting1058").setExecutor(new Reporting1058());
        getCommand("reportslist").setExecutor(new ReportsList());
        getCommand("delreports").setExecutor(new DelReports());
        getCommand("mostreported").setExecutor(new MostReported());
        getCommand("rreload").setExecutor(new ReportReload());
        new bStats(this);
        Bukkit.getPluginManager().registerEvents(new Listeners(), this);
        Updater.checkUpdates();
    }
}
